using UnityEngine;
using System.Collections;

public abstract class GameLevel : MonoBehaviour {
	
	public int mapSize;
	
	public abstract IEnumerator objectivesDone ();
	
	public void checkColors () {
		Selectionable[] selectionables = FindObjectsOfType(typeof(Selectionable)) as Selectionable[];
		foreach	(Selectionable s in selectionables) {
			s.selectionHalo.renderer.material.color = s.controller.getPlayerColor();
			s.shape.renderer.material.color = s.controller.getPlayerColor();
		}
	}
	
	protected void globalInitialize () {
		Selectionable[] sels = FindObjectsOfType(typeof(Selectionable)) as Selectionable[];
		foreach (Selectionable s in sels) {
			s.initialize();
		}
	}
	
	protected GameObject createInitialShip (string s, Vector3 pos,Player player) {
		GameObject ship = Instantiate (Resources.Load ("Ships/" + s),pos,Quaternion.identity) as GameObject;
		ship.layer = player.layer;	
		foreach ( Transform t in ship.transform ) {
			GameObject gameobj = t.gameObject;
			if ( gameobj.name != "Shape" && gameobj.name != "detect" )
				gameobj.layer = player.layer;
		}
		ship.GetComponent<Selectionable>().controller = player;
		return ship;
	}
	
	protected void createNaquadah (Vector3 pos) {
		GameObject naq = Instantiate (Resources.Load ("Naquadah/Naquadah"),pos,Quaternion.Euler (new Vector3(0,Random.Range (0,361),0))) as GameObject;	
		Player player = GameObject.Find ("Main Camera").GetComponent<Game>().players[0] as Player;
		naq.layer = 16;
		foreach ( Transform t in naq.transform ) {
			GameObject gameobj = t.gameObject;
			if ( gameobj.name != "Shape" )
				gameobj.layer = 16;
		}
		naq.GetComponent<Selectionable>().controller = player;
	}
	
	protected GameObject createInitialBuilding(string b, Vector3 pos, Player player) {
		GameObject building = Instantiate (Resources.Load ("Buildings/" + b),pos,Quaternion.identity) as GameObject;
		building.transform.root.gameObject.layer = player.layer;	
		foreach ( Transform t in building.transform.root ) {
			GameObject gameobj = t.gameObject;
			if ( gameobj.name != "Shape" && gameobj.name != "detect" )
				gameobj.layer = player.layer;
		}
		building.transform.root.GetComponent<Selectionable>().controller = player;
		building.transform.root.GetComponent<Building>().isInitialBuilding = true;
		building.transform.root.GetComponent<Building>().build();
		building.transform.root.transform.position = pos;
		
		return building;
	}
}
