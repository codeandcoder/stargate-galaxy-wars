using UnityEngine;
using System.Collections;

public class TheBeginning : GameLevel {
	
	private GameObject objective;
	
	void Awake () {
		this.mapSize = 100;
	}
	
	void Start() {
		Player p = new Player(1,1,ProfileController.getActualProfileName(),"Tau'ri",Color.cyan,9);
		Player p2 = new Player(2,2,"Player2","Goa'uld",Color.red,10);
		GameObject.Find ("Main Camera").GetComponent<Game>().players.Add (p);
		GameObject.Find ("Main Camera").GetComponent<Game>().players.Add (p2);
		this.createInitialShip("X303",new Vector3(80,0.2F,-70),p);
		this.createInitialShip("Hatak",new Vector3(-65,0.2F,80),p2);
		this.createInitialShip("Hatak",new Vector3(-80,0.2F,65),p2);
		this.createInitialBuilding("Midway",new Vector3(80,0.2F,-80),p);
		this.objective = this.createInitialBuilding("GoauldBase",new Vector3(-80,0.2F,80),p2);
		Player n = GameObject.Find ("Main Camera").GetComponent<Game>().players[0] as Player;
		this.createNaquadah(new Vector3(70,0.01F,-70));
		this.globalInitialize();
		GameObject.Find ("Camera").GetComponent<PlayerControl>().player = p;
		GameObject.Find ("Camera").transform.position = new Vector3 (77,30,-91);
		this.checkColors();
		//StartCoroutine(objectivesDone());
		StartCoroutine(tutorial());
	}
	
	public override IEnumerator objectivesDone () {
		bool isFinished = false;
		bool winGame = false;
		while (!isFinished) {
			Building[] builds = GameObject.FindObjectsOfType(typeof(Building)) as Building[];
			ArrayList enemyBuilds = new ArrayList();
			ArrayList allyBuilds = new ArrayList();
			foreach ( Building b in builds ) {
				if ( b.controller.getPlayerNumber() == 2 )
					enemyBuilds.Add(b);
				else 
					allyBuilds.Add(b);
			}
			
			if ( enemyBuilds.Count == 0 ) {
				isFinished = true;
				winGame = true;
			} else if ( allyBuilds.Count == 0 ) {
				isFinished = true;
				winGame = false;
			}
			
			yield return new WaitForSeconds(1);
		}
		
		if ( winGame )
			Interface.winGame();
		else {
			Interface.failGame();	
		}
	}
	
	public IEnumerator tutorial () {
		Interface.notification("Bienvenido a Stargate Galaxy Wars",5,Color.blue,'c');
		yield return new WaitForSeconds(2.5F);
		Interface.notification("controlar todos los aspectos del juego",10,Color.blue,'c');
		Interface.notification("En este tutorial aprenderás a ",10,Color.blue,'c');
		yield return new WaitForSeconds(5);
		Interface.notification("sobre el edifício que rota sobre sí mismo.",10,Color.blue,'c');
		Interface.notification("Para empezar, selecciona tu base haciendo clic",10,Color.blue,'c');
		Interface.objective = "Selecciona tu base (el edifício que rota sobre sí mismo).";
		while ( GameObject.Find ("Camera").GetComponent<PlayerControl>().selectionArray.Count < 1 
			|| !(GameObject.Find ("Camera").GetComponent<PlayerControl>().selectionArray[0] is MidWayStation) ) {
			
			yield return new WaitForSeconds(0.2F);
		}
		Interface.objective = null;
		Interface.cleanNotifications();
		Interface.notification("selecciona tu nave de la misma forma.",20,Color.blue,'c');
		Interface.notification("cuando estan cerca. Ahora",20,Color.blue,'c');
		Interface.notification("y la tripulación de tus naves",20,Color.blue,'c');
		Interface.notification("Bien hecho! Las bases regenerán el casco",20,Color.blue,'c');
		Interface.objective = "Selecciona tu nave.";
		while ( GameObject.Find ("Camera").GetComponent<PlayerControl>().selectionArray.Count < 1 
			|| !(GameObject.Find ("Camera").GetComponent<PlayerControl>().selectionArray[0] is Prometheus) ) {
			
			yield return new WaitForSeconds(0.2F);
		}
		Interface.objective = null;
		Interface.cleanNotifications();
		Interface.notification("izquierda del icono de la nave.",30,Color.blue,'c');
		Interface.notification("se encuentra en la parte superior ",30,Color.blue,'c');
		Interface.notification("haciendo clic en la \"I\" que",30,Color.blue,'c');
		Interface.notification("Puedes ver mas información",30,Color.blue,'c');
		Interface.notification("casco, escudos y energía respectivamente.",30,Color.blue,'c');
		Interface.notification("ver sus niveles de tripulaciín,",30,Color.blue,'c');
		Interface.notification("Bien hecho! Abajo a la izquierda puedes",30,Color.blue,'c');
		Interface.objective = "Abre el menú de información de tu nave (La \' I \' en la parte superior izquierda del icono de la nave).";
		while ( !Interface.informationSelection ) {
			
			yield return new WaitForSeconds(0.2F);
		}
		Interface.objective = null;
		Interface.cleanNotifications();
		Interface.notification("Arriba puedes ver tus existencias de Naquadah.",10,Color.blue,'c');
		Interface.notification("Bien hecho!",10,Color.blue,'c');
		
		yield return new WaitForSeconds(5);
		
		Interface.notification("donde se ponga de color verde.",30,Color.blue,'c');
		Interface.notification("el icono del Stargate y colócalo cerca del Naquadah",30,Color.blue,'c');
		Interface.notification("para deseleccionarlo todo, haz clic sobre",30,Color.blue,'c');
		Interface.notification("Haz clic en cualquier lugar del mapa",30,Color.blue,'c');
		Interface.notification("de Naquadah, como ese cúmulo de asteroides.",30,Color.blue,'c');
		Interface.notification("debes crear Stargates cerca de depósitos",30,Color.blue,'c');
		Interface.notification("Para hacer que aumenten más rápido",30,Color.blue,'c');
		Interface.objective = "Sin nada seleccionado, haz clic sobre el icono del Stargate y posiciónalo.";
		while ( GameObject.Find("Stargate(Clone)") == null || GameObject.Find("Stargate(Clone)").GetComponent<Building>().buildingState != Building.BuildingStates.done  ) {
			
			yield return new WaitForSeconds(1);
		}
		Interface.objective = null;
		Interface.cleanNotifications();
		Interface.notification("Bien hecho!",5,Color.blue,'c');
		
		yield return new WaitForSeconds(2.5F);
		
		Player p = GameObject.Find ("Camera").GetComponent<PlayerControl>().player;
		GameObject ship = this.createInitialShip("Hatak",new Vector3(80,0.2F,0),p);
		ship.GetComponent<Selectionable>().initialize();
		yield return new WaitForSeconds(1);
		Player p2 = GameObject.Find ("Main Camera").GetComponent<Game>().players[2] as Player;
		ship.GetComponent<Ship>().controller = p2;
		ship.GetComponent<Ship>().actualHull /= 5;
		foreach ( Transform t in ship.transform ) {
			GameObject gameobj = t.gameObject;
			if ( gameobj.name != "Shape" && gameobj.name != "detect" )
				gameobj.layer = p2.layer;
		}
		this.checkColors();
		
		Interface.notification("Házlo sobre la nave enemiga para destruirla.",30,Color.blue,'c');
		Interface.notification("sobre cualquier lugar del espacio para moverla.",30,Color.blue,'c');
		Interface.notification("Con tu nave seleccionada haz clic derecho",30,Color.blue,'c');
		Interface.notification("cerca de tu posición, al norte.",30,Color.blue,'c');
		Interface.notification("Se ha detectado una nave enemiga estropeada",30,Color.blue,'c');
		Interface.objective = "Destruye la nave enemiga al norte de tu base.";
		while ( ship != null ) {
			yield return new WaitForSeconds(1);
		}
		Interface.objective = null;
		Interface.notification("Bien hecho!",5,Color.blue,'c');
		
		yield return new WaitForSeconds(2.5F);
		
		Interface.notification("nave enemiga quede inutilizada; se llama modo captura.",30,Color.blue,'c');
		Interface.notification("que si muestra un circulo parará de atacar cuando la",30,Color.blue,'c');
		Interface.notification("Si muestra una cruz atacará a destruir mientras",30,Color.blue,'c');
		Interface.notification("Haz clic en el para cambiar el modo de ataque.",30,Color.blue,'c');
		Interface.notification("superior derecha del icono de tu nave?",30,Color.blue,'c');
		Interface.notification("Ves ese boton con la cruz en la parte",30,Color.blue,'c');
		
		yield return new WaitForSeconds(15);
		
		ship = this.createInitialShip("Hatak",new Vector3(60,0.01F,-70),p);
		ship.GetComponent<Selectionable>().initialize();
		yield return new WaitForSeconds(1);
		ship.GetComponent<Ship>().controller = p2;
		foreach ( Transform t in ship.transform ) {
			GameObject gameobj = t.gameObject;
			if ( gameobj.name != "Shape" && gameobj.name != "detect" )
				gameobj.layer = p2.layer;
		}
		this.checkColors();
		
		Interface.notification("la nave enemiga.",15,Color.blue,'c');
		Interface.notification("Pon tu nave en modo captura y envíala a atacar",15,Color.blue,'c');
		Interface.notification("Una nave está intentando capturar tu Stargate!",15,Color.blue,'c');
		Interface.objective = "Defiende tu Stargate capturando la nave enemiga que está junto a él (pon tu nave en modo captura).";
		while ( ship.GetComponent<Ship>().actualTripulation > 0 ) {
			yield return new WaitForSeconds(1);
		}
		Interface.objective = null;
		Interface.notification("modo captura para pasarle la mitad de tu tripulación.",20,Color.blue,'c');
		Interface.notification("inutilizada enemiga con tu nave seleccionada en",20,Color.blue,'c');
		Interface.notification("tu nave, pero antes, haz clic derecho sobre la nave",20,Color.blue,'c');
		Interface.notification("a ser neutral. Puedes recapturarlo acercando",20,Color.blue,'c');
		Interface.notification("Como podras apreciar, tu Stargate ha pasado",20,Color.blue,'c');
		Interface.notification("Bien hecho!",20,Color.blue,'c');
		Interface.objective = "Haz clic derecho en modo captura sobre la nave derrotada para capturarla.";
		while ( ship.GetComponent<Ship>().actualTripulation < 10 ) {
			yield return new WaitForSeconds(1);
		}
		Interface.objective = null;
		Interface.cleanNotifications();
		Interface.notification("a tu base para que regenerase casco y tripulacion.",20,Color.blue,'c');
		Interface.notification("Tambien seria recomendable que acercaras tu nueva nave",20,Color.blue,'c');
		Interface.notification("ahora tienes una nave tuya cerca.",20,Color.blue,'c');
		Interface.notification("Tu Stargate comenzara a recapturarse ya que.",20,Color.blue,'c');
		Interface.notification("Bien hecho!",20,Color.blue,'c');
		
		yield return new WaitForSeconds(10);
		
		Interface.notification("superior izquierda del mapa, buena suerte!",20,Color.blue,'c');
		Interface.notification("con la base enemiga que se encuentra en la parte",20,Color.blue,'c');
		Interface.notification("Cuando hayas acabado, organiza tu flota y acaba",20,Color.blue,'c');
		
		objective.GetComponent<Selectionable>().controller = p;
		foreach ( Transform t in objective.transform ) {
			GameObject gameobj = t.gameObject;
			if ( gameobj.name != "Shape" && gameobj.name != "detect" )
				gameobj.layer = p.layer;
		}
		yield return new WaitForSeconds(1);
		objective.GetComponent<Selectionable>().controller = p2;
		foreach ( Transform t in objective.transform ) {
			GameObject gameobj = t.gameObject;
			if ( gameobj.name != "Shape" && gameobj.name != "detect" )
				gameobj.layer = p2.layer;
		}
		Interface.objective = "Destruye la base enemiga con tus dos naves.";
		while ( objective != null ) {
			yield return new WaitForSeconds(1);
		}
		
		Interface.winGame();
	}
	
}
