using UnityEngine;
using System.Collections;

public class SoundDestroy : MonoBehaviour {

	void Start () {
		StartCoroutine( destroySound() );
	}
	
	private IEnumerator destroySound () {
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		Destroy (gameObject);
	}

}
