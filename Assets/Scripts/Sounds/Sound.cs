using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {

	public static GameObject playSound( string sound, Vector3 place, float volume ) {
		GameObject s = Instantiate(Resources.Load ("Sounds/sound"),place,Quaternion.identity) as GameObject;
		s.GetComponent<AudioSource>().clip = Resources.Load ("Sounds/files/" + sound) as AudioClip;
		s.GetComponent<AudioSource>().volume = volume;
		s.GetComponent<AudioSource>().Play();
		return s;
	}
}
