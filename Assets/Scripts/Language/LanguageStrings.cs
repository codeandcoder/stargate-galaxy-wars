using UnityEngine;
using System.Collections;

public abstract class LanguageStrings : MonoBehaviour {
	
	// Initialized at 100 texts, provisional.
	protected string[] texts = new string[100];
	
	public abstract void Start();
	
	public override abstract string ToString();
	
	public string getText(int id) {
		return this.texts[id];
	}
}