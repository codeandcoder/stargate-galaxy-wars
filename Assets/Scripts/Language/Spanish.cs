using UnityEngine;
using System.Collections;

public class Spanish : LanguageStrings {

	// Use this for initialization
	public override void Start () {
		this.texts[0] = "No se ha creado ning�n perfil, para jugar debes crear uno.";
		this.texts[1] = "CREAR PERFIL";
		this.texts[2] = "DESCONOCIDO";
		this.texts[3] = "PERFIL";
		this.texts[4] = "ATR�S";
		this.texts[5] = "STARGATE GALAXY";
		this.texts[6] = "JUGAR";
		this.texts[7] = "GESTI�N DE PERFILES";
		this.texts[8] = "OPCIONES";
		this.texts[9] = "BORRAR";
		this.texts[10] = "CREAR";
		this.texts[11] = "OPCIONES";
		this.texts[12] = "GR�FICOS";
		this.texts[13] = "VOLUMEN";
		this.texts[14] = "CALIDAD BAJA";
		this.texts[15] = "CALIDAD MEDIA";
		this.texts[16] = "CALIDAD ALTA";
		this.texts[17] = "SUBT�TULOS";
		this.texts[18] = "NO";
		this.texts[19] = "S�";
		this.texts[20] = "SUBT�TULOS DESACTIVADOS";
		this.texts[21] = "SUBT�TULOS ACTIVADOS";
		this.texts[22] = "M�SICA";
		this.texts[23] = "EFECTOS";
		this.texts[24] = "DIALOGOS";
		this.texts[25] = "CAMPA�A";
		this.texts[26] = "CONQUISTA DE LA GALAXIA";
		this.texts[27] = "BATALLA PERSONALIZADA";
		this.texts[28] = "MULTIJUGADOR";
		this.texts[29] = "SALIR";
		this.texts[30] = "TUTORIAL";
		this.texts[31] = "F�CIL";
		this.texts[32] = "MEDIO";
		this.texts[33] = "DIF�CIL";
		this.texts[34] = "Tutorial para aprender los principios del juego.";
		this.texts[35] = "Volver";
		this.texts[36] = "Salir al menu principal";
		this.texts[37] = "Salir del juego";
		this.texts[38] = "La nave ";
		this.texts[39] = " ha sido destruida.";
		this.texts[40] = "Casco m�ximo: ";
		this.texts[41] = "Escudos m�ximos: ";	
		this.texts[42] = "Regeneraci�n de escudo: ";
		this.texts[43] = "Regeneraci�n del casco: ";
		this.texts[44] = "Nombre: ";
		this.texts[45] = "Tripulaci�n m�nima: ";
		this.texts[46] = "HAS VENCIDO!";
		this.texts[47] = "HAS SIDO DERROTADO!";
	}
	
	public override string ToString () {
		 return "Spanish";
	}
}
