using UnityEngine;
using System.Collections;

public class English : LanguageStrings {

	// Use this for initialization
	public override void Start () {
		this.texts[0] = "There isn't any profile, you must create one to play.";
		this.texts[1] = "CREATE PROFILE";
		this.texts[2] = "UNKNOWN";
		this.texts[3] = "PROFILE";
		this.texts[4] = "BACK";
		this.texts[5] = "STARGATE GALAXY";
		this.texts[6] = "PLAY";
		this.texts[7] = "PROFILE GESTION";
		this.texts[8] = "OPTIONS";
		this.texts[9] = "DELETE";
		this.texts[10] = "CREATE";
		this.texts[11] = "OPTIONS";
		this.texts[12] = "GRAPHICS";
		this.texts[13] = "VOLUME";
		this.texts[14] = "LOW QUALITY";
		this.texts[15] = "MEDIUM QUALITY";
		this.texts[16] = "HIGHT QUALITY";
		this.texts[17] = "SUBTITLES";
		this.texts[18] = "NO";
		this.texts[19] = "YES";
		this.texts[20] = "SUBTITLES DESACTIVATED";
		this.texts[21] = "SUBTITLES ACTIVATED";
		this.texts[22] = "MUSIC";
		this.texts[23] = "EFFECTS";
		this.texts[24] = "DIALOGS";
		this.texts[25] = "CAMPAIGN";
		this.texts[26] = "GALAXY CONQUEST";
		this.texts[27] = "CUSTOM BATTLE";
		this.texts[28] = "MULTIPLAYER";
		this.texts[29] = "EXIT";
		this.texts[30] = "TUTORIAL";
		this.texts[31] = "EASY";
		this.texts[32] = "AVERAGE";
		this.texts[33] = "HARD";
		this.texts[34] = "The tutorial to learn how to play the game.";
		this.texts[35] = "Return";
		this.texts[36] = "Exit to main menu";
		this.texts[37] = "Exit game";
		this.texts[38] = "The ship ";
		this.texts[39] = " has been destroyed.";
		this.texts[40] = "Maximum Hull: ";
		this.texts[41] = "Maximum Shields: ";
		this.texts[42] = "Shield regeneration: ";
		this.texts[43] = "Hull regeneration: ";
		this.texts[44] = "Name: ";
		this.texts[45] = "Minimum tripulation: ";
		this.texts[46] = "YOU WON!";
		this.texts[47] = "YOU HAVE BEEN DEFEATED!";
	}
	
	public override string ToString () {
		 return "English";
	}
}
