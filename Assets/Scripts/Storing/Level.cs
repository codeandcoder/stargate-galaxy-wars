using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

	private int description;
	private int difficulty;
	private int levelName;
	private int sceneNumber;
	
	public Level(int lName, int difficulty, int description, int sceneNumber) {
		this.levelName = lName;
		this.difficulty = difficulty;
		this.description = description;
		this.sceneNumber = sceneNumber;
	}
	
	public bool isUnlocked () {
		int enabled = PlayerPrefs.GetInt(ProfileController.getActualProfileName() + this.levelName);
		
		if ( enabled == 1 ) 
			return true;	
		
		return false;
		
	}
	
	public int getDescription () {
		return this.description;
	}
	
	public int getDifficulty () {
		return this.difficulty;
	}
	
	public int getName () {
		return this.levelName;
	}
	
	public int getSceneNumber () {
		return this.sceneNumber;
	}
	
}
