using UnityEngine;
using System.Collections;

public class FoWagent : MonoBehaviour {
	
	public float viewRange;		// The view range of this object.

	private RaycastHit position;
	public LayerMask layerMask;
	private Selectionable selectionable;
	
	public bool isHidden;
	public bool lastTrans;
	
	void Awake () {
		selectionable = GetComponent<Selectionable>();
		this.isHidden = true;
		this.lastTrans = false;
	}
	
	void Start () {
		if ( !selectionable.controller.Equals (GameObject.Find ("Camera").GetComponent<PlayerControl>().player) ) {
			Renderer[] rends = GetComponentsInChildren<Renderer>();
			foreach (Renderer r in rends) {
				r.enabled = false;
			}
		} else {
			this.isHidden = false;	
		}
	}
	
	void Update () {

			if ( selectionable.controller.Equals (GameObject.Find ("Camera").GetComponent<PlayerControl>().player) ) {
				if ( FoWplane.getCounter() == 0 ) {
					Physics.Raycast(transform.position,Vector3.down,out position,Mathf.Infinity,layerMask);
					FoWplane.show(position.transform.InverseTransformPoint(position.point),viewRange);
				}
			} else if ( selectionable.hostile != null && selectionable.hostile.Equals (GameObject.Find ("Camera").GetComponent<PlayerControl>().player) ) {
				if ( FoWplane.getCounter() == 0 ) {
					Physics.Raycast(transform.position,Vector3.down,out position,Mathf.Infinity,layerMask);
					FoWplane.show(position.transform.InverseTransformPoint(position.point),1);
				}
			} else if ( FoWplane.getCounter() == 0 ) {
				Physics.Raycast(transform.position,Vector3.down,out position,Mathf.Infinity,layerMask);
				if ( FoWplane.allTrans(position.transform.InverseTransformPoint(position.point),GetComponent<Ship>() != null) && !lastTrans ) {
					this.isHidden = false;
					this.lastTrans = true;
					Renderer[] rends = GetComponentsInChildren<Renderer>();
					foreach (Renderer r in rends) {
						if (r.transform.name != "selectionHalo") {
							r.enabled = true;
						}
					}
				} else if ( GetComponent<Ship>() != null && !FoWplane.allTrans(position.transform.InverseTransformPoint(position.point),GetComponent<Ship>() != null) && lastTrans ) {
					this.isHidden = true;
					this.lastTrans = false;
					Renderer[] rends = GetComponentsInChildren<Renderer>();
					foreach (Renderer r in rends) {
						r.enabled = false;
					}
				}
			}
		}
		
	}