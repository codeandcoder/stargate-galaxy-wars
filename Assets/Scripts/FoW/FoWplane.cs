using UnityEngine;
using System.Collections;

public class FoWplane : MonoBehaviour {

	private static int refreshRate;
	private static int rCounter = 1;
	
	private static Mesh mesh;
	private static Vector3[] vertices;
	private static Color[] colors;
	
	void Awake () {
		
		// Initialization
		FoWplane.refreshRate = 10;
		FoWplane.mesh = GetComponent<MeshFilter>().mesh;
		FoWplane.vertices = mesh.vertices;
		FoWplane.colors = new Color[vertices.Length];
		
		for ( int i = 0; i < FoWplane.vertices.Length; i++ ) {
			FoWplane.colors[i] = new Color (0,0,0,1);
		}
		FoWplane.mesh.colors = FoWplane.colors;
	}

	void Update () {
		FoWplane.rCounter++;
		if (FoWplane.rCounter == FoWplane.refreshRate) {
			refresh();
			FoWplane.rCounter = 0;
		} else if (rCounter == 1) {
			FoWplane.mesh.colors = FoWplane.colors;	
		}
	}
	
	// Turn all iluminated vertices into semitransparent vertices.
	void refresh () {
		for ( int i = 0; i < FoWplane.vertices.Length; i++ ) {
			if (FoWplane.colors[i].a < 0.6) {
				FoWplane.colors[i].a += 0.1F;
			}
		}
	}
	
	// Turn the vertices that are in "range" distance or less from "point". 
	public static void show (Vector3 point, float range) {
		for (int i = 0; i < FoWplane.vertices.Length; i++)  {
			if ( (FoWplane.vertices[i] - point).sqrMagnitude <= range*range ) {
				FoWplane.colors[i].a = 0;
			}
		}
	}
	
	// Return true if the "point" is in a iluminated area.
	public static bool allTrans (Vector3 point,bool isShip) {
		bool isAllTransparent = false;
		bool finish = false;		
		float range = isShip ? 0.01F : 0.025F;
		
		for (int i = 0; i < FoWplane.vertices.Length && !finish; i++)  {
			if ( (FoWplane.vertices[i] - point).sqrMagnitude <= range*range ) {
				if ( FoWplane.colors[i].a < 0.6 ) {
					finish = true;
					isAllTransparent = true;
				}
			}
		}

		return isAllTransparent;
	}
	
	public static int getCounter () {
		return FoWplane.rCounter;	
	}
}
