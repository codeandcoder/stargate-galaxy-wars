using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {
	
	private MenuScreen actualMenu;
	public Texture mainBackground;
	public Texture secondBackground;
	public Texture titleTex;
	public GUISkin menuSkin;
	public GUIStyle titleFont;
	public Texture spanish;
	public Texture english;
	public Texture tick;
	public Game game;
	public AudioSource music;
	public AudioSource click;
	
	// This is the inicializtion of the game, when we start in the menu.
	void Start () {
		
		// Creates a new Game.
		this.game = gameObject.AddComponent( "Game" ) as Game;
		
		// Checks and sets the active and existing profiles.
		ProfileController.configure();
		
		// Sets the initial menu.;
		if (this.actualMenu == null || !(this.actualMenu is ProfileMenu))
			this.actualMenu = gameObject.AddComponent( "MainMenu" ) as MenuScreen;
		
		StartCoroutine(checkVolumes ());
		
	}
	
	void Update () {
		if ( Input.GetKey (KeyCode.Mouse0) ) {
			this.click.Play();	
		}
	}
	
	private IEnumerator checkVolumes () {
		this.music = GetComponent<AudioSource>();
		this.click = transform.Find ("clickSound").GetComponent<AudioSource>();
		
		float mVol = -1;
		float effVol = -1;
		
		while ( true ) {
			if ( mVol != this.game.musicVol ) {
				mVol = this.game.musicVol;
				this.music.volume = mVol / 100;
			}
			
			if ( effVol != this.game.effectsVol ) {
				effVol = this.game.effectsVol;
				this.click.volume = effVol / 100;
			}
			
			yield return new WaitForSeconds (0.5F);
		}
	}
	
	public void changeScreen(string screen) {
		Destroy (actualMenu);
		this.actualMenu = gameObject.AddComponent( screen ) as MenuScreen;	
	}
	
	public LanguageStrings changeLanguage(string language) {
		this.game.changeLang (language);
		return this.game.getSettedLanguage();
	}

	public void exitGame() {
		Application.Quit();
	}

	public void startGame() {
		Application.LoadLevel(this.game.getLevel());
		Destroy (actualMenu);
		Destroy (GetComponent<Camera>());
		Destroy (GetComponent<AudioListener>());
		Destroy (GetComponent<AudioSource>());
		Destroy (transform.Find ("clickSound").gameObject);
		Destroy (this);
	}

}
