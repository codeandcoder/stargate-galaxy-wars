using UnityEngine;
using System.Collections;

public abstract class MenuScreen : MonoBehaviour {
	
	public LanguageStrings infoTexts;
	protected MenuController menuController;

	public void Start() {
		this.menuController = GetComponent ("MenuController") as MenuController;
		this.infoTexts = GetComponent ("LanguageStrings") as LanguageStrings;
	}

	public abstract void OnGUI();
	
}
