using UnityEngine;
using System.Collections;

public class CampaignMenu : MenuScreen {
	
	private bool levelsLoaded = false;
	
	public override void OnGUI() {
		
		if (!levelsLoaded) {
			Levels.loadLevels();
			this.levelsLoaded = true;
		}
		
		GUI.skin = this.menuController.menuSkin;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), this.menuController.secondBackground, ScaleMode.StretchToFill, true, 0);
		
		GUI.Label (new Rect(Screen.width/2 - 150,20,300,100), this.infoTexts.getText (25));
		
		for (int i = 0; i < Levels.levels.Count; i++) {
			Level lvl = Levels.levels[i] as Level;
			if ( lvl.isUnlocked() ) {
				if ( GUI.Button (new Rect(Screen.width / 2 - 100, 160 + 40*i,200,30),new GUIContent(this.infoTexts.getText(lvl.getName()),this.infoTexts.getText(lvl.getDescription()))) ) {
					this.menuController.game.setLevel(lvl.getSceneNumber());
					this.menuController.startGame();	
				}
			} else {
				GUI.Label (new Rect(Screen.width / 2 - 100, 160 + 30*i,200,30),this.infoTexts.getText(lvl.getName()));
			}
		}
		
		if (!GUI.tooltip.Equals("")) {
			GUI.Box (new Rect (Screen.width/6,Screen.height - Screen.height/4,Screen.width / 6 * 4,Screen.height/4 - 70), GUI.tooltip);	
		}
		
		if (GUI.Button (new Rect(Screen.width/2 - 50, Screen.height - 60, 100, 40), this.infoTexts.getText (4))) {
			this.menuController.changeScreen("GameSelectionMenu");	
		}
		
	}
}
