using UnityEngine;
using System.Collections;

public class MainMenu : MenuScreen {
	
	public override void OnGUI() {
		GUI.skin = this.menuController.menuSkin;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), this.menuController.mainBackground, ScaleMode.StretchToFill, true, 0);
		
		GUI.DrawTexture(new Rect(Screen.width/6,40,Screen.width/6*4,Screen.height / 5), this.menuController.titleTex);
		/*GUI.Label (new Rect(Screen.width/6,40,Screen.width/6*4,Screen.height / 7),"STARGATE GALAXY", this.menuController.titleFont);
		GUI.Label (new Rect(Screen.width/6,40 + Screen.height / 7,Screen.width/6*4,Screen.height / 7),"WARS", this.menuController.titleFont);*/
		
		if ( GUI.Button (new Rect(Screen.width/4 -50 ,Screen.height/3*2.25F,100,50),this.infoTexts.getText (6)) ) {
			this.menuController.changeScreen("GameSelectionMenu");	
		}
		
		if ( GUI.Button (new Rect(Screen.width/2 -100, Screen.height/3*2.25F, 200, 50), this.infoTexts.getText (7)) ) {
			ProfileController.changeActualProfile();
		}
		
		if ( GUI.Button (new Rect(Screen.width/4*3 -50 ,Screen.height/3*2.25F,100,50),this.infoTexts.getText (8)) ) {
			this.menuController.changeScreen("OptionsMenu");	
		}
		
		if (GUI.Button (new Rect(Screen.width/2 - 50, Screen.height - 60, 100, 40), this.infoTexts.getText (29))) {
			Application.Quit ();	
		}
		
		GUI.Label (new Rect(Screen.width - 230,Screen.height - 40, 115, 30), this.infoTexts.getText (3) + "    -");
		GUI.Label (new Rect(Screen.width - 115,Screen.height - 40, 115, 30), ProfileController.getActualProfileName());
		
		/*if ( GUI.Button (new Rect(10, Screen.height -90, 70, 70), this.menuController.spanish) ) {
			this.infoTexts = this.menuController.changeLanguage("Spanish");
			PlayerPrefs.SetString(ProfileController.getActualProfileName() + "Language", "Spanish");
			PlayerPrefs.Save ();
		}
		if ( GUI.Button (new Rect(90, Screen.height -90, 70, 70), this.menuController.english) ) {
			this.infoTexts = this.menuController.changeLanguage("English");
			PlayerPrefs.SetString(ProfileController.getActualProfileName() + "Language", "English");
			PlayerPrefs.Save ();
		}*/
	}
}

	

