using UnityEngine;
using System.Collections;

public class OptionsMenu : MenuScreen {

	public override void OnGUI() {
		GUI.skin = this.menuController.menuSkin;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), this.menuController.secondBackground, ScaleMode.StretchToFill, true, 0);
		
		GUI.Label (new Rect(Screen.width/2 - 150,20,300,100), this.infoTexts.getText (11));
		
		// Options for graphics.
		GUI.Label (new Rect(Screen.width/7 - 40, Screen.height/2 - 150, 175, 30), this.infoTexts.getText (12));
		if (GUI.Button(new Rect(Screen.width/7 - 25, Screen.height/2 - 70, 150, 50), this.infoTexts.getText (14))) {
			this.menuController.game.graphics = 0;
			QualitySettings.SetQualityLevel(0);
			PlayerPrefs.SetInt (ProfileController.getActualProfileName() + "Graphics", 0);
			
		}
		if (GUI.Button(new Rect(Screen.width/7 - 25, Screen.height/2 - 20, 150, 50), this.infoTexts.getText (15))) {
			this.menuController.game.graphics = 3;
			QualitySettings.SetQualityLevel(3);
			PlayerPrefs.SetInt (ProfileController.getActualProfileName() + "Graphics", 3);
		}
		if (GUI.Button(new Rect(Screen.width/7 - 25, Screen.height/2 + 30, 150, 50), this.infoTexts.getText (16))) {
			this.menuController.game.graphics = 5;
			QualitySettings.SetQualityLevel(5);
			PlayerPrefs.SetInt (ProfileController.getActualProfileName() + "Graphics", 5);
		}
		
		// Options for volume.
		GUI.Label (new Rect(Screen.width/2 - 75, Screen.height/2 - 150, 175, 30), this.infoTexts.getText (13));
		// MUSIC
		GUI.Label (new Rect(Screen.width/2 - 185, Screen.height/2 - 80, 100, 30), this.infoTexts.getText (22));
		this.menuController.game.musicVol = GUI.HorizontalSlider (new Rect (Screen.width/2 - 65, Screen.height/2 - 70, 150, 20), this.menuController.game.musicVol, 0.0F, 100.0F);
		GUI.Label (new Rect(Screen.width/2 + 90, Screen.height/2 - 75, 50, 20), Mathf.Round(this.menuController.game.musicVol) + "%");
		// EFFECTS
		GUI.Label (new Rect(Screen.width/2 - 185, Screen.height/2 - 30, 100, 30), this.infoTexts.getText (23));
		this.menuController.game.effectsVol = GUI.HorizontalSlider (new Rect (Screen.width/2 - 65, Screen.height/2 - 20, 150, 20), this.menuController.game.effectsVol, 0.0F, 100.0F);
		GUI.Label (new Rect(Screen.width/2 + 90, Screen.height/2 - 25, 50, 20), Mathf.Round(this.menuController.game.effectsVol) + "%");
		// DIALOGS
		GUI.Label (new Rect(Screen.width/2 - 185, Screen.height/2 + 20, 100, 30), this.infoTexts.getText (24));
		this.menuController.game.dialogsVol = GUI.HorizontalSlider (new Rect (Screen.width/2 - 65, Screen.height/2 + 20, 150, 20), this.menuController.game.dialogsVol, 0.0F, 100.0F);
		GUI.Label (new Rect(Screen.width/2 + 90, Screen.height/2 + 25, 50, 20), Mathf.Round(this.menuController.game.dialogsVol) + "%");
		
		// Options for subtitles.
		GUI.Label (new Rect(Screen.width/2 + 300, Screen.height/2 - 150, 175, 30), this.infoTexts.getText (17));
		if (GUI.Button(new Rect(Screen.width/2 + 340, Screen.height/2 - 90, 40, 35), this.infoTexts.getText (19))) {
			this.menuController.game.subs = true;
			PlayerPrefs.SetInt (ProfileController.getActualProfileName() + "Subtitles", 1);
		}
		
		if (GUI.Button(new Rect(Screen.width/2 + 390, Screen.height/2 - 90, 40, 35), this.infoTexts.getText (18))) {
			this.menuController.game.subs = false;
			PlayerPrefs.SetInt (ProfileController.getActualProfileName() + "Subtitles", 0);
		}
		
		// Conditional to activate the subtitle's label.
		if (this.menuController.game.subs) {
			GUI.Label (new Rect(Screen.width/2 + 310, Screen.height/2 - 30, 150, 35), this.infoTexts.getText(21));
		} else {
			GUI.Label (new Rect(Screen.width/2 + 310, Screen.height/2 - 30, 150, 35), this.infoTexts.getText(20));
		}
		
		if (this.menuController.game.graphics == 0) {
			GUI.DrawTexture (new Rect(Screen.width/7 + 130, Screen.height/2 - 50, 20, 20), this.menuController.tick);
		} else if (this.menuController.game.graphics == 3) {
			GUI.DrawTexture (new Rect(Screen.width/7 + 130, Screen.height/2, 20, 20), this.menuController.tick);
		} else if (this.menuController.game.graphics == 5) {
			GUI.DrawTexture (new Rect(Screen.width/7 + 130, Screen.height/2 + 50, 20, 20), this.menuController.tick);
		}
		
		// Return to the main menu.
		if (GUI.Button (new Rect(Screen.width/2 - 50, Screen.height - 60, 100, 40), this.infoTexts.getText (4))) {
			PlayerPrefs.SetInt (ProfileController.getActualProfileName() + "MusicVolume", (int)this.menuController.game.musicVol);
			PlayerPrefs.SetInt (ProfileController.getActualProfileName() + "EffectsVolume", (int)this.menuController.game.effectsVol);
			PlayerPrefs.SetInt (ProfileController.getActualProfileName() + "DialogsVolume", (int)this.menuController.game.dialogsVol);
			PlayerPrefs.Save ();
			this.menuController.changeScreen("MainMenu");
		}
	}
	
}
