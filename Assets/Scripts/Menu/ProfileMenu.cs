using UnityEngine;
using System.Collections;

public class ProfileMenu : MenuScreen {
	
	private bool nameInitialized = false;
	
	private string[] profileNames;
	private bool[] ticks = new bool[5];
	
	public override void OnGUI() {
		GUI.skin = this.menuController.menuSkin;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), this.menuController.secondBackground, ScaleMode.StretchToFill, true, 0);
		
		GUI.Label (new Rect(Screen.width/2 - 150,20,300,100), this.infoTexts.getText (7));
		
		if ( !nameInitialized ) {
			profileNames = new string[5];
			for ( int i = 0; i < 5; i++ ) {
				profileNames[i] = infoTexts.getText (2);
			}
			nameInitialized = true;
		}
		
		if ( !PlayerPrefs.HasKey ("LastProfile") ) {
			
			GUI.Label( new Rect(Screen.width / 3,Screen.height / 8, Screen.width / 3, 40), this.infoTexts.getText (0) );
			profileNames[0] = GUI.TextField( new Rect(Screen.width / 3, Screen.height / 3, Screen.width / 3, 30), profileNames[0]);
			
			if ( GUI.Button(new Rect(Screen.width / 5 * 2,Screen.height / 2, Screen.width / 5, 30), this.infoTexts.getText (1)) ) {
				if ( !profileNames[0].Equals("") && !profileNames[0].Equals(" ") ) {
					PlayerPrefs.SetString("Profile1",profileNames[0]);
					PlayerPrefs.SetInt (profileNames[0] + 30,1);
					PlayerPrefs.SetString("LastProfile","Profile1");
					PlayerPrefs.Save();
					this.menuController.game.setActualProfile("Profile1");
					this.menuController.game.varLoads(PlayerPrefs.GetString (PlayerPrefs.GetString("LastProfile")));
					this.menuController.changeScreen("MainMenu");
				}
			}
			
		} else {
			for ( int i = 1; i < 6; i++) {
				GUI.Label( new Rect(Screen.width / 6,Screen.height / 20 + (Screen.height / 10 + 30) * i-1 , Screen.width / 12, 30), this.infoTexts.getText (3) + i + ":");
				if ( PlayerPrefs.HasKey("Profile" + i) ) {
					if ( GUI.Button( new Rect(Screen.width / 7 * 2,Screen.height / 20 + (Screen.height / 10 + 30) * i-1 , Screen.width / 12, 30), PlayerPrefs.GetString("Profile" + i)) ) {
						setTicksOff();
						this.ticks[i-1] = true;
						PlayerPrefs.SetString("LastProfile","Profile" + i);
						PlayerPrefs.Save();
						this.menuController.game.setActualProfile("Profile" + i);
					}
					if ( GUI.Button (new Rect (Screen.width / 6 + Screen.width / 7 + Screen.width / 12,Screen.height / 20 + (Screen.height / 10 + 30) * i-1 , 70, 30), this.infoTexts.getText (9))) {
						if ( countProfiles() > 1 ) {
							PlayerPrefs.DeleteKey("Profile" + i);
							PlayerPrefs.Save ();
						}
					}
				} else {
					profileNames[i-1] = GUI.TextField( new Rect(Screen.width / 7 * 2,Screen.height / 20 + (Screen.height / 10 + 30) * i-1 , Screen.width / 12, 30), profileNames[i-1] );
					if ( GUI.Button (new Rect (Screen.width / 6 + Screen.width / 7 + Screen.width / 12,Screen.height / 20 + (Screen.height / 10 + 30) * i-1 , 70, 30), this.infoTexts.getText (10))) {
						if ( !profileNames[i-1].Equals("") && !profileNames[i-1].Equals(" ") ) {
							PlayerPrefs.SetString("Profile" + i,profileNames[i-1]);
							PlayerPrefs.SetInt (profileNames[i-1] + 30,1);
							PlayerPrefs.SetString("LastProfile","Profile" + i);
							PlayerPrefs.Save();
							this.menuController.game.setActualProfile("Profile" + i);
							this.menuController.changeScreen("MainMenu");
						}
					}
				}
			}
			if ( GUI.Button (new Rect(Screen.width / 3 * 2, Screen.height / 2, 70, 30),this.infoTexts.getText(4))) {
				if ( !PlayerPrefs.HasKey(PlayerPrefs.GetString("LastProfile")) ) {
					PlayerPrefs.SetString("LastProfile",getExistingProfile());
					PlayerPrefs.Save();
					this.menuController.game.setActualProfile(getExistingProfile());
				}
				this.menuController.game.varLoads(PlayerPrefs.GetString (PlayerPrefs.GetString("LastProfile")));
				this.menuController.changeScreen("MainMenu");	
			}
		}
		
		for (int i = 1; i < 6; i++) {
			if (PlayerPrefs.GetString("LastProfile").Equals ("Profile" + i)) {
				GUI.DrawTexture (new Rect(Screen.width / 6 + Screen.width / 7 + Screen.width / 12 + 80, Screen.height / 20 + (Screen.height / 10 + 30) * i-1, 20, 20), this.menuController.tick);				
			}
		}
	
	}
	
	private int countProfiles () {
		int counter = 0;
		for ( int i = 1; i < 6; i++) {
			if ( PlayerPrefs.HasKey("Profile" + i) ) {
				counter++;	
			}
		}
		return counter;
	}
	
	private string getExistingProfile () {
		for ( int i = 1; i < 6; i++) {
			if ( PlayerPrefs.HasKey("Profile" + i) ) {
				return "Profile" + i;
			}
		}
		
		return null;
	}
	
	private void setTicksOff () {
		for (int i = 0; i < 5; i++) {
			this.ticks[i] = false;	
		}
	}
}
