using UnityEngine;
using System.Collections;

public class ProfileController : MonoBehaviour {
	
	private static MenuController menuController;
	
	public static void configure () {
		
		ProfileController.menuController = GameObject.Find ("Main Camera").GetComponent ("MenuController") as MenuController;
		
		if ( !PlayerPrefs.HasKey ("LastProfile") ) {
			ProfileController.menuController.changeScreen ("ProfileMenu");
		} else {
			ProfileController.menuController.game.setActualProfile(PlayerPrefs.GetString ("LastProfile"));
		}
		
		// Load the profile vars.
		ProfileController.menuController.game.varLoads(getActualProfileName());
	}
	
	public static void changeActualProfile() {
		ProfileController.menuController.changeScreen ("ProfileMenu");
	}
			
	public static string getActualProfileName() {
		return PlayerPrefs.GetString(ProfileController.menuController.game.getActualProfile());			
	}

}
