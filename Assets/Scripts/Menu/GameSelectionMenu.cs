using UnityEngine;
using System.Collections;

public class GameSelectionMenu : MenuScreen {

	public override void OnGUI() {
		GUI.skin = this.menuController.menuSkin;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), this.menuController.mainBackground, ScaleMode.StretchToFill, true, 0);
		
		GUI.DrawTexture(new Rect(Screen.width/2 - 200,40,400,60), this.menuController.titleTex);
		
		if ( GUI.Button (new Rect(Screen.width/9 -50 ,Screen.height/3*2.25F,100,50),this.infoTexts.getText (25)) ) {
			this.menuController.changeScreen("CampaignMenu");	
		}
		
		/*if ( GUI.Button (new Rect(Screen.width/9*3 -125, Screen.height/3*2.25F, 250, 50), this.infoTexts.getText (26)) ) {
			this.menuController.changeScreen("GalaxyConquestMenu");
		}*/
		GUI.Label(new Rect(Screen.width/9*3 -125, Screen.height/3*2.25F, 250, 50), this.infoTexts.getText (26));
		/*if ( GUI.Button (new Rect(Screen.width/9*5 -125 ,Screen.height/3*2.25F,250,50),this.infoTexts.getText (27)) ) {
			this.menuController.changeScreen("CustomBattleMenu");	
		}*/
		GUI.Label(new Rect(Screen.width/9*5 -125 ,Screen.height/3*2.25F,250,50),this.infoTexts.getText (27));
		/*if ( GUI.Button (new Rect(Screen.width/9*7 -75 ,Screen.height/3*2.25F,150,50),this.infoTexts.getText (28)) ) {
			this.menuController.changeScreen("MultiplayerMenu");	
		}*/
		GUI.Label(new Rect(Screen.width/9*7 -75 ,Screen.height/3*2.25F,150,50),this.infoTexts.getText (28));
		if (GUI.Button (new Rect(Screen.width/2 - 50, Screen.height - 60, 100, 40), this.infoTexts.getText (4))) {
			this.menuController.changeScreen("MainMenu");	
		}
	}
}
