using UnityEngine;
using System.Collections;

public class ShipAI : MonoBehaviour {
	
	void OnTriggerStay(Collider other) {
		Selectionable sel = other.transform.root.GetComponent<Selectionable>();
		if ( !other.isTrigger && other.transform.name.Equals("basicHull") && sel != null ) {
			if ( sel.controller.getTeam() != GetComponent<Selectionable>().controller.getTeam()
				&& sel.gameObject.layer != 16
				&& !(sel is Stargate) 
				&& (GetComponent<Ship>().pWeapons[0] as Weapon).objective == null
				&& Mathf.Abs(GetComponent<NavMeshAgent>().destination.x - transform.position.x) <= 1
				&& Mathf.Abs(GetComponent<NavMeshAgent>().destination.z - transform.position.z) <= 1
				&& (!(sel is Building) || sel is Building && (sel as Building).buildingState == Building.BuildingStates.done)
				&& !GetComponent<Ship>().inhabilited) {
				
				GetComponent<Ship>().primaryWeaponsFire(sel.transform.gameObject);
			}
		}
	}
	
	void Update () {
		if ( GetComponent<Selectionable>().hostile != null && (GetComponent<Ship>().pWeapons[0] as Weapon).objective == null 
			&& Mathf.Abs(GetComponent<NavMeshAgent>().destination.x - transform.position.x) <= 1
			&& Mathf.Abs(GetComponent<NavMeshAgent>().destination.z - transform.position.z) <= 1
			&& !GetComponent<Ship>().inhabilited) {
			
			GetComponent<Ship>().primaryWeaponsFire(GetComponent<Selectionable>().hostile);	
		}
	}
}
