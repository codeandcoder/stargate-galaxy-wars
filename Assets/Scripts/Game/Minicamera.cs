using UnityEngine;
using System.Collections;

public class Minicamera : MonoBehaviour {
	
	public enum CamStates { Max, Min }
	public CamStates camState = CamStates.Min;
	
	public LayerMask layerMask;
	public Texture borderTex;
	private static RaycastHit clic;
	private static Rect area;
	private static int actualWidth;
	
	void Start () {
		camera.pixelRect = new Rect(Screen.width * 0.02F,Screen.height - Screen.width * 0.02F - Screen.width * 0.1F,Screen.width * 0.1F,Screen.width * 0.1F);
		actualWidth = Screen.width;
		Minicamera.area = camera.pixelRect;
	}
	
	void Update () {;
		if ( Screen.width != actualWidth ) {
			area = camera.pixelRect = new Rect(Screen.width * 0.02F,Screen.height - Screen.width * 0.02F - Screen.width * 0.1F,Screen.width * 0.1F,Screen.width * 0.1F);
			camState = CamStates.Min;
			actualWidth = Screen.width;
		}
		if ( Input.GetKeyDown (KeyCode.M) ) {
			if ( camState == CamStates.Min ) {
				area = camera.pixelRect = new Rect(Screen.width * 0.35F,Screen.height - Screen.width * 0.25F - Screen.width * 0.1F,Screen.width * 0.3F,Screen.width * 0.3F);
				camState = CamStates.Max;	
			} else {
				area = camera.pixelRect = new Rect(Screen.width * 0.02F,Screen.height - Screen.width * 0.02F - Screen.width * 0.1F,Screen.width * 0.1F,Screen.width * 0.1F);
				camState = CamStates.Min;
			}
		}
		
		if ( isInMinimap(Input.mousePosition) && Input.GetKeyDown (KeyCode.Mouse0) ) {
			Physics.Raycast (camera.ScreenPointToRay(Input.mousePosition),out clic,Mathf.Infinity,layerMask);
			Object[] cameras = GameObject.FindObjectsOfType(typeof(Camera));
			foreach (Object c in cameras) {
				Camera mainCamera = c as Camera;
				if ( mainCamera != camera) {
					mainCamera.transform.position = new Vector3 (clic.point.x,mainCamera.transform.position.y,clic.point.z);
				}
			}
		}
		if ( isInMinimap(Input.mousePosition) && Input.GetKeyDown (KeyCode.Mouse1) ) {
			Physics.Raycast (camera.ScreenPointToRay(Input.mousePosition),out clic,Mathf.Infinity,layerMask);
			for (int i = 0; i < GameObject.Find ("Camera").GetComponent<PlayerControl>().selectionArray.Count; i++) {
					Selectionable c = GameObject.Find ("Camera").GetComponent<PlayerControl>().selectionArray[i] as Selectionable;
					if ( GameObject.Find ("Camera").GetComponent<PlayerControl>().player.Equals(c.controller) ) {
						// If the selected object is a ship and we have clicked the floor, move the ship to the click position.
						if ( clic.transform.name.Equals ("Base") ) {
							if ( c.transform.GetComponent<Ship>() ) {
								c.GetComponent<Ship>().move(clic.point, false);
							}
						// If we clicked another ship, the selected ship attacks it.
						} else if ( c.GetComponent<Ship>() ){
							c.GetComponent<Ship>().primaryWeaponsFire(clic.transform.gameObject);
						}
					}
			}
		}
	}
	
	void OnGUI () {
		if ( camState == CamStates.Max ) {
			GUI.DrawTexture( new Rect (	area.xMin - 42, Screen.height - area.yMax - 42, camera.pixelWidth + 84, camera.pixelHeight + 84), borderTex );
		} else if ( camState == CamStates.Min ) {
			GUI.DrawTexture( new Rect (	area.xMin - 14, Screen.height - area.yMax - 14, camera.pixelWidth + 28, camera.pixelHeight + 28), borderTex );	
		}
	}
	
	public static bool isInMinimap (Vector3 point) {
		return area.Contains (point);
	}

}
