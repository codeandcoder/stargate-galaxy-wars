using UnityEngine;
using System.Collections;

public abstract class Building : Selectionable {
	
	public enum BuildingStates { placeSelection, done }
	public BuildingStates buildingState;
	
	public bool rightPlace;
	
	public float maxHull;
	public float actualHull;
	public float hullRegen;
	
	public bool isInitialBuilding = false;
	public bool isInCombat = false;
	public float lastImpact;
	
	protected float naqRating;
	protected float incomingFreq;
	protected float timer = 0;
	
	public abstract override void initialize();
	
	public override void getDamage (float damage) {
		PlayerControl pC = GameObject.Find ("Camera").GetComponent<PlayerControl>() as PlayerControl;
		if ( !this.isInCombat && this.controller.Equals(pC.player) )
			Interface.notification(this.selectName + " esta siendo atacado.",5,Color.red,'l');
		this.isInCombat = true;
		this.lastImpact = Time.time;
		
		if ( this.actualHull >= 0 ) {
			this.actualHull -= damage;
		}
			
		if ( this.actualHull < 0 )
			this.buildDestroy();
	}
	
	protected void regenerations() {
		if ( !this.isInCombat) {
			if ( this.actualHull < this.maxHull ) {
				this.actualHull += this.hullRegen * Time.deltaTime;
			} else if ( this.actualHull > this.maxHull ) {
				this.actualHull = this.maxHull;
			}
		} else if (Time.time - this.lastImpact > 5) {
			this.isInCombat = false;
		}
		
	}
	
	public void buildDestroy () {
		Sound.playSound( "NearExplosionA",transform.position,1);
		Destroy(GetComponent<Rigidbody>());
		foreach ( Rigidbody r in transform.GetComponentsInChildren<Rigidbody>() ) {
			r.isKinematic = false;
		}
		PlayerControl pC = GameObject.Find ("Camera").GetComponent<PlayerControl>() as PlayerControl;
		pC.selectionArray.Remove(this as Selectionable);
		Interface.hideBar ();
		Instantiate (Resources.Load("Explosions/bigExplosion"),transform.position,Quaternion.identity);
		if ( pC.player.Equals(this.controller) )
			Interface.notification(Interface.lang.getText(38) + this.selectName + Interface.lang.getText(39),5,Color.red,'c');
		gameObject.AddComponent ( "ShipDestroy" );
		Destroy (this);
	}
	
	protected void checkNaqIncoming () {
		if ( this.buildingState == BuildingStates.done && Time.time - timer >= incomingFreq ) {
			timer = Time.time;
			this.controller.sumNaquadah(naqRating);
		}
	}
	
	public void build () {
		this.controller.sumNaquadah(-this.getCost());
		this.buildingState = BuildingStates.done;
		this.changeColor(Color.white);
		foreach ( Collider c in transform.GetComponentsInChildren<Collider>() ) {
			c.enabled = true;
			if ( c.transform.name != "detect" && c.transform.name != "range" )
				c.isTrigger = false;
		}
		transform.FindChild("obstacle").GetComponent<NavMeshObstacle>().enabled = true;
		GetComponent<FoWagent>().enabled = true;
	}
	
	protected void placeSelection () {
		RaycastHit hit;
		Ray direction = GameObject.Find ("Camera").camera.ScreenPointToRay(Input.mousePosition);
		LayerMask layerMask = GameObject.Find ("Camera").GetComponent<PlayerControl>().layerMask;
		Physics.Raycast(direction,out hit,Mathf.Infinity,layerMask);
		transform.position = new Vector3(hit.point.x,0.2F,hit.point.z);
		
		this.rightPlace = this.isRightPlace(hit);
		
		if ( rightPlace ) {
			this.changeColor(Color.green);	
		} else {
			this.changeColor(Color.red);
		}
	}
			
	private void changeColor (Color c) {
		foreach ( Renderer r in GetComponentsInChildren<Renderer>() ) {
			if (!r.transform.name.Equals("Shape"))
				r.material.color = c;		
		}
	}
	
	protected abstract bool isRightPlace (RaycastHit hit);
	
	public abstract GameObject instantiateBuilding(Player player);
	
	public abstract override Texture getImage ();
	
	public abstract override int getCost();

}
