using UnityEngine;
using System.Collections;

public class Naquadah : Building {
	
	private ArrayList rot;
	public Building extractor;
	
	public static Texture t = Resources.Load ("InterfaceTextures/naquadah") as Texture;
	
	public static int cost = 0;
	
	public override int getCost ()
	{
		return Naquadah.cost;
	}
	
	public override Texture getImage ()
	{
		return Naquadah.t;
	}
	
	void Start () {
		this.initialize();
		this.rot = new ArrayList();
		foreach ( Transform t in GetComponentsInChildren<Transform>() ) {
			if ( t.name != "selectionHalo" && t.name != "Shape" && t.name != "Naquadah" )
				this.rot.Add(new Vector3(Random.Range(-2F,2F)*Time.deltaTime,Random.Range(-2F,2F)*Time.deltaTime,Random.Range(-2F,2F)*Time.deltaTime));
		}
	}
	
	void Update () {
		int i = 0;
		foreach ( Transform t in GetComponentsInChildren<Transform>() ) {
			if ( t.name != "selectionHalo" && t.gameObject.layer == 16 && t.name != "Naquadah(Clone)" ) {
				t.Rotate((Vector3)rot[i],Space.World);
				i++;
			}
		}
	}
	
	public override void initialize () {
		this.extractor = null;
		this.buildingState = Building.BuildingStates.done;
		this.selectionableIntialization();
		this.naqRating = 0;
		this.incomingFreq = -1;
	}
	
	protected override bool isRightPlace (RaycastHit hit) {
		return true;
	}
	
	public override GameObject instantiateBuilding (Player player) {
		// not used
		return null;
	}
	
}
