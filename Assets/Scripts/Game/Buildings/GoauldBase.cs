using UnityEngine;
using System.Collections;

public class GoauldBase : Building {

	public static Texture t = Resources.Load ("InterfaceTextures/goauldBase") as Texture;
	
	public static int cost = 550;
	
	public override int getCost ()
	{
		return GoauldBase.cost;
	}
	
	public override Texture getImage ()
	{
		return GoauldBase.t;
	}
	
	void Start () {
		this.initialize();
	}
	
	void Update () {
		this.checkSelection();
		if ( this.selectionState == Selectionable.SelectionStates.selected ) {
			this.makeHaloRotation();
		}
		
		if (this.buildingState == Building.BuildingStates.placeSelection)
			this.placeSelection();
		
		this.checkNaqIncoming();
		if (this.buildingState == Building.BuildingStates.done) {
			foreach ( Transform t in GetComponentsInChildren<Transform>() ) {
				if ( t.name != "selectionHalo" && t.name != "Shape" && t.name != "GoauldBase(Clone)")
					t.Rotate(new Vector3(0,10*Time.deltaTime,0),Space.World);
			}
		}
	}
	
	public override void initialize () {
		if ( this.isInitialBuilding )
			this.buildingState = Building.BuildingStates.done;
		else
			this.buildingState = Building.BuildingStates.placeSelection;
		
		this.selectionableIntialization();
		this.naqRating = 2;
		this.incomingFreq = 1;
	}
	
	protected override bool isRightPlace (RaycastHit hit) {
		return hit.transform != null && (hit.transform.root.name.Equals("Base"))
			&& FoWplane.allTrans(GameObject.Find ("Fog").transform.InverseTransformPoint(transform.position.x,0,transform.position.z),true);
	}
	
	public override GameObject instantiateBuilding (Player player) {
		GameObject mw;
		mw = Instantiate (Resources.Load ("Buildings/GoauldBase"), new Vector3 (0,0,0), Quaternion.identity) as GameObject;
		foreach ( Transform t in mw.transform ) {
			if ( t.name != "Shape" )
				t.gameObject.layer = player.layer;
			}
		mw.GetComponent<Selectionable>().controller = player;
		mw.layer = player.layer;
		mw.transform.FindChild("detect").gameObject.layer = 0;
		
		return mw;
	}
}
