using UnityEngine;
using System.Collections;

public class SourceDetection : MonoBehaviour {
	
	public bool sourceAvailable;
	public Naquadah source;
	
	void Start () {
		this.sourceAvailable = false;
	}
	
	void OnTriggerStay(Collider other) {
		if ( other.transform.root.GetComponent<Naquadah>() != null ) {
			if ( !sourceAvailable ) {
				this.sourceAvailable = true;
				this.source = other.transform.root.GetComponent<Naquadah>();
			}
		}
	}
	
	void OnTriggerExit(Collider other) {
		if ( other.transform.root.GetComponent<Naquadah>() != null ) {
			this.sourceAvailable = false;
			this.source = null;
		}
	}
}
