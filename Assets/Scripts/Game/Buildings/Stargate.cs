using UnityEngine;
using System.Collections;

public class Stargate : Building {

	public static Texture t = Resources.Load ("InterfaceTextures/spaceGate") as Texture;
	
	public static int cost = 175;
	
	public override int getCost ()
	{
		return Stargate.cost;
	}
	
	private bool sourceCatched;
	
	public Range range;
	
	public override Texture getImage () {
		return Stargate.t;
	}
	
	void Start () {
		this.initialize();
	}
	
	void Update () {
		this.checkSelection();
		if ( this.selectionState == Selectionable.SelectionStates.selected ) {
			this.makeHaloRotation();
		}
		
		if (this.buildingState == Building.BuildingStates.placeSelection)
			this.placeSelection();
		
		if (this.buildingState == Building.BuildingStates.done && !this.sourceCatched) {
			this.sourceCatched = true;
			transform.Find("detect").GetComponent<SourceDetection>().source.extractor = this;
		}
		
		this.checkNaqIncoming();
		if (this.buildingState == Building.BuildingStates.done) {
			foreach ( Transform t in GetComponentsInChildren<Transform>() ) {
				if ( t.name != "selectionHalo" && t.name != "Shape" && t.name != "Stargate(Clone)")
					t.Rotate(new Vector3(0,0,10*Time.deltaTime),Space.World);
			}
		}
	}
	
	public override void initialize () {
		this.range = transform.FindChild("range").GetComponent<Range>();
		this.sourceCatched = false;
		if ( this.isInitialBuilding )
			this.buildingState = Building.BuildingStates.done;
		else
			this.buildingState = Building.BuildingStates.placeSelection;
		
		this.selectionableIntialization();
		this.naqRating = 2;
		this.incomingFreq = 1;
	}
	
	protected override bool isRightPlace (RaycastHit hit) {
		return hit.transform != null && (hit.transform.root.name.Equals("Base")) && transform.Find("detect").GetComponent<SourceDetection>().sourceAvailable
			&& FoWplane.allTrans(GameObject.Find ("Fog").transform.InverseTransformPoint(transform.position.x,0,transform.position.z),true)
			&& transform.Find("detect").GetComponent<SourceDetection>().source.extractor == null;
	}
	
	public override GameObject instantiateBuilding (Player player) {
		GameObject mw;
		mw = Instantiate (Resources.Load ("Buildings/Stargate"), new Vector3 (0,0,0), Quaternion.identity) as GameObject;
		foreach ( Transform t in mw.transform ) {
			if ( t.name != "Shape" )
				t.gameObject.layer = player.layer;
			}
		mw.GetComponent<Selectionable>().controller = player;
		mw.layer = player.layer;
		mw.transform.FindChild("detect").gameObject.layer = 0;
		mw.transform.FindChild("range").gameObject.layer = 0;
		
		return mw;
	}
}
