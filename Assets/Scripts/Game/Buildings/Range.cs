using UnityEngine;
using System.Collections;

public class Range : MonoBehaviour {

	public float conversionState;
	
	public float[] conversionPoints;
	
	public Range () {
		this.conversionState = 100;
	}
	
	void Start () {
		int plys = GameObject.Find("Main Camera").GetComponent<Game>().players.Count;
		this.conversionPoints = new float[plys];
	}
	
	void Update () {
		if ( transform.root.GetComponent<Stargate>().buildingState == Building.BuildingStates.done ) {
		for ( int i = 0; i < conversionPoints.Length; i++ ) {
				float points = this.conversionPoints[i];
				if ( points >= 100 && !transform.root.GetComponent<Stargate>().controller.Equals (GameObject.Find("Main Camera").GetComponent<Game>().players[i] as Player)) {
					transform.root.GetComponent<Stargate>().controller = GameObject.Find("Main Camera").GetComponent<Game>().players[i] as Player;
					transform.Find ("selectionHalo").renderer.material.color = transform.root.GetComponent<Stargate>().controller.getPlayerColor();
					transform.Find ("Shape").renderer.material.color = transform.root.GetComponent<Stargate>().controller.getPlayerColor();
					this.conversionState = 100;
					points = 0;
				} else if ( points > 0 ) {
					points -= 1 * Time.deltaTime;
				} else {
					points = 0;
				}
				this.conversionPoints[i] = points;
		}
		
		if ( this.conversionState < 100 && this.conversionState > 0 ) {
			this.conversionState += 1 * Time.deltaTime;	
		} else if ( this.conversionState <= 0 ) {
			this.conversionState = 100;
			transform.root.GetComponent<Stargate>().controller = GameObject.Find("Main Camera").GetComponent<Game>().players[0] as Player;
			transform.root.FindChild ("selectionHalo").renderer.material.color = transform.root.GetComponent<Stargate>().controller.getPlayerColor();
			transform.root.FindChild ("Shape").renderer.material.color = transform.root.GetComponent<Stargate>().controller.getPlayerColor();
		}
		}
	}
	
	void OnTriggerStay(Collider other) {
		if ( transform.root.GetComponent<Stargate>().buildingState == Building.BuildingStates.done ) {
		if ( other.transform.name == "basicHull" && !other.isTrigger && other.transform.root.GetComponent<Selectionable>() != null && other.transform.root.GetComponent<Selectionable>() is Ship
				&& other.transform.root.GetComponent<Ship>().tripulationState != Ship.TripulationStates.Empty) {
			int id = other.transform.root.GetComponent<Selectionable>().controller.getPlayerNumber();
			Player otherCont = other.transform.root.GetComponent<Selectionable>().controller;
			Player cont = transform.root.GetComponent<Stargate>().controller;
			if ( otherCont.getTeam() != cont.getTeam() ) {
				if ( cont.Equals (GameObject.Find("Main Camera").GetComponent<Game>().players[0]) ) {
					if ( this.conversionPoints[id] < 100 ) {
						float points = this.conversionPoints[id];
						points += 2 * Time.deltaTime;
						this.conversionPoints[id] = points;
					} else {
						this.conversionPoints[id] = 100;
					}
				} else {
					this.conversionState -= 2 * Time.deltaTime;
				}
			} else if ( this.conversionState < 100 ) {
				this.conversionState += 2 * Time.deltaTime;
			} else {
				this.conversionState = 100;	
			}
		}
		}
		
	}
	
}
