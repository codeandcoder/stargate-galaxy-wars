using UnityEngine;
using System.Collections;

public class ShipRegen : MonoBehaviour {

	private Building b;
	
	void Start () {
		this.b = transform.root.GetComponent<Building>();	
	}
	
	void OnTriggerStay (Collider other) {
		if ( b.buildingState == Building.BuildingStates.done && !other.isTrigger && other.transform.name == "basicHull" ) {
		Ship ship = other.transform.root.GetComponent<Ship>();
		if ( ship != null && b.controller.getTeam() == ship.controller.getTeam() ) {
			if ( ship.actualTripulation < ship.maxTripulation ) {
				if ( Time.time % 2 == 0 )
					ship.actualTripulation += 1;
			} else if ( ship.actualTripulation != ship.maxTripulation ){
				ship.actualTripulation = ship.maxTripulation;
			}
			
			if ( !ship.isInCombat ) {
			
				if ( ship.actualHull < ship.maxHull ) {
					ship.actualHull += ship.maxHull * 5 / 100 * Time.deltaTime;
				} else if ( ship.actualHull != ship.maxHull ) {
					ship.actualHull = ship.maxHull;		
				}
					
			}
		}
					
		}
	}
	
}
