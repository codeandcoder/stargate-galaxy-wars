using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

	private LanguageStrings lang;
	private int gameLevel;
	public string actualProfile;
	public int graphics;
	public bool subs;
	public float musicVol;
	public float effectsVol;
	public float dialogsVol;
	
	public ArrayList players;
	
	private void Start () {
		this.gameLevel = 0;
		this.players = new ArrayList();
		players.Add (new Player(0,0,"Neutral","Goa'uld",Color.grey,8));
		DontDestroyOnLoad(this);
	}
	
	
	public void changeLang (string lang) {
		Destroy(this.lang);
		this.lang = transform.gameObject.AddComponent( lang ) as LanguageStrings;
		if (GetComponent("MenuScreen")) {
			(GetComponent ("MenuScreen") as MenuScreen).infoTexts = this.lang;
		}
	}
		
	public LanguageStrings getSettedLanguage() {
		return this.lang;
	}

	public string getInfo (int id) {
		return this.lang.getText(id);
	}
	
	// Starts all the saved settings.
	public void varLoads(string profileName) {
		// Control if there is a Language PlayerPref created to change the language.
		if (PlayerPrefs.HasKey(profileName + "Language")) {
			changeLang(PlayerPrefs.GetString(profileName + "Language"));
		} else {
			changeLang("Spanish");
		}
		
		if (PlayerPrefs.HasKey (profileName + "Graphics")) {
			this.graphics = PlayerPrefs.GetInt (profileName + "Graphics");
		} else {
			this.graphics = 3;
		}
		
		if (PlayerPrefs.HasKey (profileName + "MusicVolume")) {
			this.musicVol = PlayerPrefs.GetInt (profileName + "MusicVolume");
		} else {
			this.musicVol = 100;	
		}
		
		if (PlayerPrefs.HasKey (profileName + "EffectsVolume")) {
			this.effectsVol = PlayerPrefs.GetInt (profileName + "EffectsVolume");
		} else {
			this.effectsVol = 100;	
		}
		
		if (PlayerPrefs.HasKey (profileName + "DialogsVolume")) {
			this.dialogsVol = PlayerPrefs.GetInt (profileName + "DialogsVolume");
		} else {
			this.dialogsVol = 100;	
		}
		
		if (PlayerPrefs.HasKey(profileName + "Subtitles")) {
			if (PlayerPrefs.GetInt (profileName + "Subtitles") == 0) {
				this.subs = false;
			} else {
				this.subs = true;
			}
		} else {
			this.subs = false;
		}
		
		QualitySettings.SetQualityLevel(this.graphics);
			
	}
	
	// Getters

	public int getLevel() {
		return this.gameLevel;		
	}

	public string getActualProfile () {
		return this.actualProfile;
	}

	// Setters

	public void setLevel(int gameLevel) {
		this.gameLevel = gameLevel;		
	}

	public void setActualProfile(string profile) {
		this.actualProfile = profile;
	}
}
