using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	private int playerNumber;
	private string playerName;
	private Color playerColor;
	private int team;
	public Faction faction;
	
	private float Naquadah;
	
	public int layer;
	
	public Player (int num, int team, string pName, string faction, Color pColor, int layer) {
		this.playerNumber = num;
		this.team = team;
		this.playerName = pName;
		this.faction = new Faction(faction);
		this.playerColor = pColor;
		this.layer = layer;
		this.Naquadah = 350;
	}
	
	public int getTeam() {
		return this.team;	
	}
	
	public int getPlayerNumber () {
		return this.playerNumber;	
	}
	
	public string getPlayerName () {
		return this.playerName;	
	}
	
	public Color getPlayerColor () {
		return this.playerColor;	
	}
	
	public void setTeam (int num) {
		this.team = num;	
	}
	
	public void setPlayerNumber (int num) {
		this.playerNumber = num;	
	}
	
	public void setPlayerName (string name) {
		this.playerName = name;	
	}
	
	public void setPlayerColor (Color color) {
		this.playerColor = color;	
	}
	
	public void sumNaquadah (float num) {
		this.Naquadah += num;	
	}
	
	public float getNaquadah () {
		return this.Naquadah;	
	}
	
	public override bool Equals (object o)
	{
		if ( o == null ) 
			return false;
		
		Player other = o as Player;
		return this.playerNumber == other.playerNumber;
	}
	
	public override int GetHashCode ()
	{
		return playerNumber.GetHashCode ();
	}
}
