using UnityEngine;
using System.Collections;

public class Faction : MonoBehaviour {

	private string factionName;
	public Building[] buildings;
	public Ship[] ships;
	
	public Faction (string factionName) {
		this.factionName = factionName;
		this.buildFaction();
	}
	
	private void buildFaction () {
		switch (this.factionName) {
		case "Tau'ri":
			this.buildings = new Building[2];
			this.buildings[0] = new MidWayStation();
			this.buildings[1] = new Stargate();
			this.ships = new Ship[2];
			this.ships[0] = new Prometheus();
			this.ships[1] = new Daedalus();
			break;
		case "Goa'uld":
			this.buildings = new Building[2];
			this.buildings[0] = new GoauldBase();
			this.buildings[1] = new Stargate();
			this.ships = new Ship[1];
			this.ships[0] = new Hatak();
			break;
		default:
			this.factionName = null;
			break;
		}
	}
	
	public string getFactionName() {
		return this.factionName;	
	}
	
}
