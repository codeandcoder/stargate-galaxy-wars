using UnityEngine;
using System.Collections;

public class Notification : MonoBehaviour {
	
	// The propeties of the Notification.
	public string text;
	public double counter;
	public Color color;
	public char location;

	public Notification (string text, double counter,Color color,char location) {
		this.text = text;
		this.counter = counter;
		this.color = color;
		this.location = location;
	}
}
