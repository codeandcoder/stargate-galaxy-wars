using UnityEngine;
using System.Collections;

public abstract class Selectionable : MonoBehaviour {

	public enum SelectionStates { unselected, selected }
	public SelectionStates selectionState;
	
	public SelectionStates lastSelectionState;
	
	public Transform selectionHalo;
	public Transform shape;
	
	public Player controller;
	
	public string selectName;
	
	private float attackSelectionTime = 0;
	
	public GameObject hostile;
	
	public abstract void initialize ();
	
	public abstract void getDamage(float damage);
	
	public abstract Texture getImage();
	
	public abstract int getCost();
	
	protected void makeSelection() {
		if ( this.selectionHalo != null ) {
			selectionHalo.renderer.enabled = true;
			selectionHalo.renderer.material.color = controller.getPlayerColor();
			shape.renderer.material.color = Color.white;
		}
	}
	
	protected void makeUnselection() {
		if ( this.selectionHalo != null ) {
			selectionHalo.renderer.enabled = false;
			shape.renderer.material.color = controller.getPlayerColor();
		}
	}
	
	protected void checkSelection() {
		if ( this.lastSelectionState != this.selectionState ) {
			if ( this.selectionState == SelectionStates.selected ) {
				this.makeSelection();	
			} else {
				this.makeUnselection();
			}
		}
		
		this.lastSelectionState = this.selectionState;
	}
	
	protected void selectionableIntialization () {
		this.selectionState = SelectionStates.unselected;
	}
	
	protected void makeHaloRotation () {
		if ( this.selectionHalo != null ) {
			this.selectionHalo.transform.Rotate (0,20*Time.deltaTime,0);
		}
	}
	
	public void attackSelection () {
		StartCoroutine(selectionBeh ());
		this.attackSelectionTime = Time.time;
	}
	
	public IEnumerator selectionBeh () {
		GameObject hal = Instantiate (Resources.Load ("halAttack"),transform.position,transform.rotation) as GameObject;
		for (int i = 0; i < 20; i++ ) {
			hal.transform.localScale = hal.transform.localScale - new Vector3(0.025F,0.025F,0.025F);
			hal.transform.Translate(new Vector3(0,0.1F,0));
			yield return new WaitForSeconds (0.001F);
		}
		Destroy (hal);

	}
	
	private void OnMouseOver () {
		if ( !GetComponent<FoWagent>().isHidden ) {
			Interface.showBar(this as Selectionable);
		}
	}
	
	private void OnMouseExit () {
		Interface.hideBar();
	}

	
}
