using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {
		
	public Vector3 objective;
	
	// Use this for initialization
	void Start () {
		Sound.playSound("teleport",transform.position,0.6F);
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
		transform.LookAt(objective);
		GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0,0,850));
		if ( Vector3.Distance(transform.position,objective) <= 0.5F ) {
			GetComponent<ParticleEmitter>().emit = false;
			Destroy (gameObject);
		}
	}
}
