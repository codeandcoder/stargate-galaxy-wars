using UnityEngine;
using System.Collections;

public class Missile : Projectile {
	
	void Start () {
		this.setTime();
		Sound.playSound( "Weapons/MissileFire",transform.position,0.3F);
	}
	
	void Update () {
		this.checkTime();
		if ( this.objective != null && this.objective.GetComponent<Selectionable>() != null ) {
			Quaternion rotAux = transform.rotation;
			transform.LookAt (this.objective.transform.position);
			Quaternion targetRot = transform.rotation;
			transform.rotation = rotAux;
			if (transform.rotation != targetRot) {
				transform.rotation = Quaternion.Slerp (rotAux,targetRot,6F*Time.deltaTime);
			}
			rigidbody.velocity = Vector3.zero;
			rigidbody.AddRelativeForce (Vector3.forward * 750);
		} else {
			Destroy (gameObject);	
		}
	}
	
	protected override IEnumerator destruction () {
		Destroy (GetComponent<Collider>());
		Destroy (GetComponent<Light>());
		transform.FindChild ("missile").GetComponentInChildren<Renderer>().enabled = false;
		GetComponent<ParticleEmitter>().emit = false;
		yield return new WaitForSeconds (1);
		Destroy (gameObject);
	}
	
}
