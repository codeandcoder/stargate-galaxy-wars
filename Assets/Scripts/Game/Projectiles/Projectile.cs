using UnityEngine;
using System.Collections;

public abstract class Projectile : MonoBehaviour {
	
	public GameObject origin;
	
	public float power;
	
	public Player controller;
	
	public GameObject objective;
	
	public float lifeTime;
	
	private float time;
	
	void OnCollisionEnter (Collision c) {
		Selectionable sel = c.collider.transform.root.GetComponent<Selectionable>() as Selectionable;
		if ( sel != null ) {
			if ( sel is Ship && (sel as Ship).actualShield > 0 ) {
				GameObject shield = Instantiate((sel as Ship).shieldImpact,transform.position,transform.rotation) as GameObject;
				Sound.playSound( "shieldHit",transform.position,0.5F);
				shield.transform.LookAt(c.collider.transform.position);
				shield.transform.parent = c.collider.transform;
			} else if ( sel is Ship && !((sel as Ship).actualShield > 0) || sel is Building ) {
				GameObject exp = Instantiate(Resources.Load ("Explosions/normalExplosion"),transform.position,Quaternion.Euler(270,0,0)) as GameObject;
				Sound.playSound( "NearExplosionB",transform.position,0.5F);
				exp.transform.parent = c.collider.transform;
			}
			if ( sel is Ship && (sel as Ship).actualHull >= 0 || sel is Building && !(sel is Naquadah) && (sel as Building).actualHull >= 0 )
				sel.getDamage(this.power);
				
			StartCoroutine(destruction());
			sel.hostile = this.origin;
		}
	}
	
	protected void setTime () {
		this.time = Time.time;	
	}
	
	protected void checkTime () {
		if ( Time.time - this.time >= this.lifeTime )
			Destroy (gameObject);
	}
	
	protected abstract IEnumerator destruction ();

}
