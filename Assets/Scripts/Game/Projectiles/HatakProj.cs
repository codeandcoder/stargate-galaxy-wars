using UnityEngine;
using System.Collections;

public class HatakProj : Projectile {
	
	private Vector3 point;
	
	private Quaternion fixedRotation;
	
	void Start () {
		Sound.playSound( "Weapons/HatakPulse",transform.position,0.3F);
		this.setTime();
		if (this.objective != null && this.objective.GetComponent<Selectionable>() != null) {
			Vector3 randomVector = new Vector3 (0,0,0);
			if ( this.objective.GetComponent<Selectionable>() is Ship && this.objective.GetComponent<Ship>().actualShield > 0 ) {
				randomVector = new Vector3(Random.Range(-1,2),Random.Range(-1,2),Random.Range(-1,2));
			}
			this.point = this.objective.transform.position + randomVector;
			transform.LookAt(this.point);
			this.fixedRotation = transform.rotation;
			rigidbody.AddRelativeForce(Vector3.forward * 1000);
		} else {
			Destroy (gameObject);
		}
	}
	
	void Update () {
		
		if ( transform.rotation != this.fixedRotation )
			transform.rotation = this.fixedRotation;
		
		this.checkTime();
		if ( this.objective.GetComponent<Selectionable>() == null ) {
			Destroy (gameObject);
		}
	}
	
	protected override IEnumerator destruction () {
		Destroy (gameObject);
		yield return new WaitForSeconds (0.05F);
	}

}
