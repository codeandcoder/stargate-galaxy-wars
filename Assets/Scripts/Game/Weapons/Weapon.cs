using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour {
	
	public float energyCost;
	public GameObject projectile;
	public GameObject objective;
	public float fireRate;
	
	protected Ship ship;
	
	public float range;
	
	public abstract void fire (GameObject objective);
	
	protected IEnumerator encarate () {
		while ( this.objective != null ) {
			transform.root.GetComponent<NavMeshAgent>().Stop ();
			transform.root.GetComponent<NavMeshAgent>().updateRotation = false;
			Quaternion rotAux = transform.root.rotation;
			transform.root.LookAt (this.objective.transform.position);
			Quaternion targetRot = transform.root.rotation;
			transform.root.rotation = rotAux;
			if (transform.root.rotation != targetRot) {
				transform.root.rotation = Quaternion.Slerp (rotAux,targetRot,1*Time.deltaTime);
			}
			yield return new WaitForSeconds (0.001F);
		}
	}
	
	protected bool capture () {
		return transform.root.GetComponent<Ship>().attackState == Ship.AttackStates.Capturing;
	}
}
