using UnityEngine;
using System.Collections;

public class MissileWeapon : Weapon {
	
	private float timer = -20;
	
	void Start () {
		this.ship = transform.root.GetComponent<Ship>();
		this.fireRate = 0.3F; 	
	}
	
	
	public override void fire (GameObject objective) {
		
		if ( this.objective == null && Time.time - timer >= (4 + this.fireRate * 2) ) {
			this.objective = objective;
			StartCoroutine(encarate());
			StartCoroutine(internalFire ());
		} else if ( Time.time - timer < (4 + this.fireRate * 2) ) {
			this.objective = objective;
			StartCoroutine(encarate());
		} else {
			this.objective = objective;
		}
	}
	
	private IEnumerator internalFire () {
		while ( this.objective != null && this.objective.GetComponent<Selectionable>() != null && ship.actualTripulation >= ship.minTripulation 
				&& this.objective.GetComponent<Selectionable>().controller.getTeam() != transform.root.GetComponent<Selectionable>().controller.getTeam()
				&& Vector3.Distance (this.transform.position,this.objective.transform.position) < this.range && ( !this.capture() || ( this.objective.GetComponent<Ship>() == null || ( this.objective.GetComponent<Ship>().actualTripulation > 0 ))) ) {
				for ( int i = 0; i < 8; i++ ) {
					if ( ship.actualEnergy >= this.energyCost ) {
						ship.actualEnergy -= this.energyCost;
						ship.isInCombat = true;
						GameObject p = Instantiate (this.projectile,transform.position,Quaternion.Euler (transform.rotation.x,transform.rotation.y + Random.Range (-90,91),transform.rotation.z)) as GameObject;
						p.layer = transform.root.GetComponent<Selectionable>().controller.layer + 10;
						foreach ( Collider c in transform.root.GetComponentsInChildren<Collider>() ) {
							if (c.enabled)
								Physics.IgnoreCollision(c,p.GetComponent<Collider>());
						}
						p.GetComponent<Projectile>().objective = this.objective;
						p.GetComponent<Projectile>().controller = transform.root.GetComponent<Selectionable>().controller;
						p.GetComponent<Projectile>().origin = transform.root.gameObject;
			
						yield return new WaitForSeconds (this.fireRate);
					}
				}
				timer = Time.time;
				yield return new WaitForSeconds ((4 + this.fireRate * 2));
		}
		this.objective = null;
		transform.root.GetComponent<NavMeshAgent>().updateRotation = true;
	}
}
