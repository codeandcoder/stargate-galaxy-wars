using UnityEngine;
using System.Collections;

public class Prometheus : Ship {
	
	public static Texture t = Resources.Load ("InterfaceTextures/Prometheus") as Texture;
	
	public static int cost = 800;
	
	public override int getCost ()
	{
		return Prometheus.cost;
	}
	
	public override Texture getImage () {
		return Prometheus.t;
	}
	
	public override void initialize () {
		this.selectionableIntialization();
		this.shipInitialize ();
	}
	
	private void Update () {
		this.checkSelection();
		if ( this.selectionState == Selectionable.SelectionStates.selected ) {
			this.makeHaloRotation();
		}
		this.regenerations();
		this.checkTripulationState();
	}
	
	public override void primaryWeaponsFire (GameObject objective) {
		this.teleportAvailable = false;
		if ( Vector3.Distance(objective.transform.position,transform.position) <= this.primaryRange ) {
			foreach ( Weapon w in this.pWeapons ) {
				w.fire (objective);
			}
		} else {
			StartCoroutine(followToAttack (objective,this.primaryRange));
		}
	}
	
	public override void secondaryWeaponsFire (GameObject objective) {
		this.isInCombat = true;
		this.lastImpact = Time.time;
		foreach ( Weapon w in this.sWeapons ) {
			this.actualEnergy -= w.energyCost;
			w.fire (objective);	
		}
	}

}
