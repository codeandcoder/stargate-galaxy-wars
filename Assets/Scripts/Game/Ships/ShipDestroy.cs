using UnityEngine;
using System.Collections;

public class ShipDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine( shipFracture() );
	}
	
	private IEnumerator shipFracture () {
		yield return new WaitForSeconds(4);
		Destroy (gameObject);
	}
}
