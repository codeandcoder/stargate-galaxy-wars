using UnityEngine;
using System.Collections;

public class Hatak : Ship {

	public static Texture t = Resources.Load ("InterfaceTextures/hatak") as Texture;
	
	public static int cost = 610;
	
	public override int getCost ()
	{
		return Hatak.cost;
	}
	
	public override Texture getImage () {
		return Hatak.t;
	}
	
	public override void initialize () {
		this.selectionableIntialization();
		this.shipInitialize ();
	}
	
	private void Update () {
		this.checkSelection();
		if ( this.selectionState == Selectionable.SelectionStates.selected ) {
			this.makeHaloRotation();
		}
		this.regenerations();
		this.checkTripulationState();
	}
	
	public override void primaryWeaponsFire (GameObject objective) {
		this.teleportAvailable = false;
		if ( Vector3.Distance(objective.transform.position,transform.position) <= this.primaryRange ) {
			StartCoroutine(this.internalPWFire(objective));
		} else {
			StartCoroutine(followToAttack (objective,this.primaryRange));
		}
	}
	
	public override void secondaryWeaponsFire (GameObject objective) {
		this.isInCombat = true;
		this.lastImpact = Time.time;
		foreach ( Weapon w in this.sWeapons ) {
			this.actualEnergy -= w.energyCost;
			w.fire (objective);	
		}
	}
	
	private IEnumerator internalPWFire (GameObject objective) {
		foreach ( Weapon w in this.pWeapons ) {
			w.fire (objective);
			yield return new WaitForSeconds (0.33F);
		}
	}
}
