using UnityEngine;
using System.Collections;

public abstract class Ship : Selectionable {
	
	public float maxHull;
	public float actualHull;
	public float maxShield;
	public float actualShield;
	public float maxEnergy;
	public float actualEnergy;
	public float energyRegen;
	public float shieldRegen;
	
	public int maxTripulation;
	public int minTripulation;
	public int actualTripulation;
	
	public enum TripulationStates { Ordinary, Decreasing, Decreased, NotEnough, Empty }
	public TripulationStates tripulationState = TripulationStates.Ordinary;
	
	public ArrayList pWeapons;
	protected ArrayList sWeapons;
	
	public Collider[] cols;
	
	public GameObject shieldImpact;
	
	public bool isTeleporting = false;
	public bool teleportAvailable = true;
	public bool isInCombat = false;
	public float lastImpact;
	public bool inhabilited = false;
	
	public enum AttackStates { Capturing, Destroying }
	public AttackStates attackState = AttackStates.Destroying;
	
	protected float primaryRange;
	protected GameObject objectiveFollowed;
	
	public abstract override void initialize ();
	
	public abstract override Texture getImage ();
	
	public abstract override int getCost();
	
	public void move (Vector3 destination, bool showMoving) {
		if (this.actualTripulation >= this.minTripulation) {
			GetComponent<NavMeshAgent>().SetDestination(destination);
			GetComponent<NavMeshAgent>().updateRotation = true;
			this.stopAttack();
			if ( Minicamera.isInMinimap(Input.mousePosition) || showMoving )
				StartCoroutine(showMove(destination));
		}
	}
	
	public abstract void primaryWeaponsFire (GameObject objective);
	
	public abstract void secondaryWeaponsFire (GameObject objective);
	
	protected void shipInitialize () {
		GetComponent<NavMeshAgent>().updateRotation = true;
		this.lastImpact = -5;
		this.pWeapons = new ArrayList();
		this.sWeapons = new ArrayList();

		this.cols = GetComponentsInChildren<Collider>() as Collider[];
		
		Weapon[] weapons = GetComponentsInChildren<Weapon>() as Weapon[];
		foreach ( Weapon w in weapons ) {
			if ( w.transform.tag == "pWeapon" ) {
				this.pWeapons.Add (w);	
			} else if ( w.transform.tag == "sWeapon" ) {
				this.sWeapons.Add (w);	
			}
		}
		
		this.primaryRange = (this.pWeapons[0] as Weapon).range;
	}
	
	public override void getDamage (float damage) {
		PlayerControl pC = GameObject.Find ("Camera").GetComponent<PlayerControl>() as PlayerControl;
		if ( Time.time - this.lastImpact > 5 && pC.player.Equals(this.controller) ) {
			Interface.notification("La nave " + this.selectName + " esta siendo atacada.",5,Color.red,'l');
		}
		this.isInCombat = true;
		this.lastImpact = Time.time;
		
		if ( this.actualShield > 0 ) {
			this.actualShield -= damage;
			if ( this.actualShield <= 0 ) {
				this.actualShield = 0;
				transform.FindChild ("shield").collider.gameObject.layer = 13;
			}
		} else if ( this.actualHull >= 0 ) {
			this.actualHull -= damage;
			float casualtiesProvability = 100 - this.actualHull / this.maxHull * 100;
			int num = Random.Range (0,100);
			if ( num <= casualtiesProvability ) {
				this.actualTripulation -= Random.Range (1,(int)(damage / 2));
			}
			
			if (this.actualTripulation < 0)
				this.actualTripulation = 0;
		}
			
		
		if ( this.actualHull < 0 )
			this.shipDestroy();
	}
	
	public void shipDestroy () {
		Destroy (GetComponent<ShipAI>());
		Sound.playSound( "NearExplosionA",transform.position,1);
		Destroy(GetComponent<Rigidbody>());
		foreach ( Rigidbody r in transform.GetComponentsInChildren<Rigidbody>() ) {
			r.isKinematic = false;
		}
		PlayerControl pC = GameObject.Find ("Camera").GetComponent<PlayerControl>() as PlayerControl;
		pC.selectionArray.Remove(this as Selectionable);
		Interface.hideBar ();
		Instantiate (Resources.Load("Explosions/bigExplosion"),transform.position,Quaternion.identity);
		if ( pC.player.Equals(this.controller) )
			Interface.notification(Interface.lang.getText(38) + this.selectName + Interface.lang.getText(39),5,Color.red,'c');
		gameObject.AddComponent ( "ShipDestroy" );
		Destroy (this);
	}
	
	protected void regenerations() {
		if ( !this.isInCombat && this.actualTripulation >= this.minTripulation) {
			if ( this.actualShield < this.maxShield ) {
				this.actualShield += this.shieldRegen * Time.deltaTime;
			} else if ( this.actualShield > this.maxShield ) {
				this.actualShield = this.maxShield;
			}
			
			if ( this.actualEnergy < this.maxEnergy ) {
				this.actualEnergy += this.energyRegen * Time.deltaTime;
			} else if ( this.actualEnergy > this.maxEnergy ) {
				this.actualEnergy = this.maxEnergy;
			}
		} else if (Time.time - this.lastImpact > 5) {
			
			this.isInCombat = false;
			this.hostile = null;
			transform.FindChild ("shield").collider.gameObject.layer = this.controller.layer;
		}
		
		if ( GetComponent<NavMeshAgent>().velocity.x <= 1 && GetComponent<NavMeshAgent>().velocity.x >= -1
			&& GetComponent<NavMeshAgent>().velocity.y <= 1 && GetComponent<NavMeshAgent>().velocity.y >= -1
			&& GetComponent<NavMeshAgent>().velocity.z <= 1 && GetComponent<NavMeshAgent>().velocity.z >= -1) {
					
			this.teleportAvailable = true;
		}
		
	}
	
	protected void checkTripulationState () {
		
		if ( this.actualTripulation >= this.maxTripulation / 2 && this.tripulationState == TripulationStates.Decreased ) {
			this.tripulationState = TripulationStates.Ordinary;
			foreach ( Weapon w in this.pWeapons ) {
				w.fireRate /= 2;
			}
		} else if (this.actualTripulation < this.maxTripulation / 2 && this.tripulationState == TripulationStates.Ordinary ) {
			this.tripulationState = TripulationStates.Decreased;
			foreach ( Weapon w in this.pWeapons ) {
				w.fireRate *= 2;
			}
		} else if (this.actualTripulation < this.minTripulation && this.tripulationState != TripulationStates.NotEnough
			&& this.tripulationState != TripulationStates.Empty) {
				
			this.tripulationState = TripulationStates.NotEnough;
			GetComponent<NavMeshAgent>().Stop();
			foreach ( Weapon w in this.pWeapons ) {
				w.objective = null;	
			}
			foreach ( Weapon w in this.sWeapons ) {
				w.objective = null;	
			}
			this.teleportAvailable = true;
			this.inhabilited = true;
		} else if ( this.actualTripulation >= this.minTripulation && (this.tripulationState == TripulationStates.NotEnough
			|| this.tripulationState == TripulationStates.Empty)) {
			this.tripulationState = TripulationStates.Decreased;
			this.inhabilited = false;	
		} else if ( this.actualTripulation == 0 && this.tripulationState != TripulationStates.Empty ) {
			if ( this.tripulationState != TripulationStates.NotEnough ) {
				GetComponent<NavMeshAgent>().Stop();
				foreach ( Weapon w in this.pWeapons ) {
					w.objective = null;	
				}
				foreach ( Weapon w in this.sWeapons ) {
					w.objective = null;	
				}
				this.teleportAvailable = true;
				this.inhabilited = true;	
			}
			this.tripulationState = TripulationStates.Empty;
		}
	}
	
	public void stopAttack() {
		foreach ( Weapon w in this.pWeapons ) {
			w.objective = null;	
		}
	}
	
	private IEnumerator showMove(Vector3 destination) {
		GameObject hal = Instantiate (Resources.Load ("halAttack"),destination + new Vector3(0,2,0),transform.rotation) as GameObject;
		hal.renderer.material.color = Color.green;
		for (int i = 0; i < 20; i++ ) {
			hal.transform.localScale = hal.transform.localScale - new Vector3(0.025F,0.025F,0.025F);
			hal.transform.Translate(new Vector3(0,-0.1F,0));
			yield return new WaitForSeconds (0.001F);
		}
		Destroy (hal);	
	}
	
	public IEnumerator follow (GameObject objective,float distance) {
		GameObject obj = objective;
		
		yield return new WaitForSeconds (0.1F);
			
			this.objectiveFollowed = obj;
			this.move( obj.transform.position, false );
			
			while ( Vector3.Distance(obj.transform.position,transform.position) > distance ) {
				yield return new WaitForSeconds (0.3F);
				this.move( obj.transform.position, false );
			}
		
			this.GetComponent<NavMeshAgent>().Stop();
	}
	
	public IEnumerator followToAttack (GameObject objective,float distance) {
		if ( !objective.Equals(this.objectiveFollowed) ) {
		
		GameObject obj = objective;
		
		yield return new WaitForSeconds (0.1F);
			
			this.objectiveFollowed = obj;
			this.move( obj.transform.position, false );
			
			while ( Vector3.Distance(obj.transform.position,transform.position) > distance ) {
				yield return new WaitForSeconds (0.3F);
				this.move( obj.transform.position, false );
			}
		
			this.primaryWeaponsFire(obj);
		}
	}
	
	public IEnumerator giveTripulation (Ship ship, int maxAmount) {
		float distance = 15;
		
		if ( this.teleportAvailable && Vector3.Distance(ship.transform.position,transform.position) > distance ) {
			StartCoroutine(follow (ship.gameObject,distance));
		}
		
		while ( Vector3.Distance(ship.transform.position,transform.position) > distance ) {
			yield return new WaitForSeconds (0.2F);
		}
		
		if (ship.controller.getTeam() != this.controller.getTeam())
			ship.capture(this.controller);
		
		if ( !this.isTeleporting ) {
			this.isTeleporting = true;
			int tripulationGiven = 0;
			while (this.teleportAvailable && this.actualTripulation > 0 && ship.actualTripulation < ship.maxTripulation && tripulationGiven < maxAmount ) {
				
				if (tripulationGiven == 0)
					StartCoroutine(teleporting(ship));
				
				tripulationGiven++;
				ship.actualTripulation += 1;
				this.actualTripulation -= 1;
				yield return new WaitForSeconds (0.5F);
				if ( Vector3.Distance(ship.transform.position,transform.position) > distance ) {
					break;
				}
			}
			this.isTeleporting = false;
		}
		yield return new WaitForSeconds (0.001F);
	}
	
	public void capture (Player player) {
		this.controller = player;
		this.selectionableIntialization();
		transform.gameObject.layer = player.layer;
		foreach ( Transform t in transform ) {
			if ( t.name != "detect" && t.name != "Shape" )
				t.gameObject.layer = player.layer;
		}
		foreach ( Weapon w in this.pWeapons ) {
			w.objective = null;	
		}
	}
	
	private IEnumerator teleporting (Ship ship) {
		while(this.isTeleporting) {
			GameObject t = Instantiate(Resources.Load ("Projectiles/teleport"),transform.FindChild("tPosition").transform.position,Quaternion.identity) as GameObject;
			t.GetComponent<Teleport>().objective = ship.transform.FindChild("tPosition").transform.position;
			yield return new WaitForSeconds(1);
		}
	}
	
}
