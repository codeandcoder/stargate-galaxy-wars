using UnityEngine;
using System.Collections;

public class Daedalus : Ship {
	
	public static Texture t = Resources.Load ("InterfaceTextures/daedalus") as Texture;
	
	public static int cost = 1800;
	
	public override int getCost ()
	{
		return Daedalus.cost;
	}
	
	public override Texture getImage () {
		return Daedalus.t;
	}
	
	public override void initialize () {
		this.selectionableIntialization();
		this.shipInitialize ();
	}
	
	private void Update () {
		this.checkSelection();
		if ( this.selectionState == Selectionable.SelectionStates.selected ) {
			this.makeHaloRotation();
		}
		this.regenerations();
		this.checkTripulationState();
	}
	
	public override void primaryWeaponsFire (GameObject objective) {
		foreach ( Weapon w in this.pWeapons ) {
			w.fire (objective);	
		}
	}
	
	public override void secondaryWeaponsFire (GameObject objective) {
		this.isInCombat = true;
		this.lastImpact = Time.time;
		foreach ( Weapon w in this.sWeapons ) {
			this.actualEnergy -= w.energyCost;
			w.fire (objective);	
		}
	}
	
}
