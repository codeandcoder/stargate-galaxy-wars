using UnityEngine;
using System.Collections;

public class Interface : MonoBehaviour {
	
	private static ArrayList notifications = new ArrayList(7);	// The notifications that can be shown.
	public static Texture baseTexture;								// The base interface texture.
	public GUISkin intSkin;
	public GUIStyle boxStyle;
	public GUIStyle littleButtonsStyle;
	public GUIStyle notificationsStyle = new GUIStyle();		// The style for the notifications.
	
	public static Texture basicBar;
	public static Texture hullBar;
	public static Texture shieldBar;
	public static Texture energyBar;
	
	private static float selTime;
	private static Rect box;
	
	public static bool menu = false;
	public static LanguageStrings lang;
	
	private static bool showInfo = false;
	private static Texture infoBar;
	private static float infoIndex;
	private static Selectionable infoSelection;
	
	public static bool buildPlacingMode = false;
	public static GameObject mw;
	
	public static Texture tick;
	public static Texture cross;
	public static Texture arrow;
	public static bool informationSelection = false;
	private Selectionable objNameSelection;
	private string newName = "New Name";
	
	public enum MapStates { Small, Big }
	public static MapStates mapState;
	public Texture minimapTex;
	
	public static string objective;
	
	void Awake () {
		baseTexture = Resources.Load ("InterfaceTextures/InterfazSGWv3") as Texture;
		basicBar = Resources.Load ("InterfaceTextures/basicBar") as Texture;
		hullBar = Resources.Load ("InterfaceTextures/hullBar") as Texture;
		shieldBar = Resources.Load ("InterfaceTextures/shieldBar") as Texture;
		energyBar = Resources.Load ("InterfaceTextures/energyBar") as Texture;
		tick = Resources.Load ("InterfaceTextures/tick") as Texture;
		cross = Resources.Load ("InterfaceTextures/cross") as Texture;
		arrow = Resources.Load ("InterfaceTextures/Flecha") as Texture;
		mapState = MapStates.Small;
		lang = GameObject.Find ("Main Camera").GetComponent<LanguageStrings>();
	}
	
	void OnGUI () {
		GUI.skin = intSkin;
		
		// Basic interface.
		GUI.DrawTexture(new Rect(0,Screen.height / 4 * 3,Screen.width,Screen.height / 4 + 3),baseTexture);
		
		GUI.Label ( new Rect ( Screen.width / 2 - 75, 0, 150,20), "Naquadah: " + GetComponent<PlayerControl>().player.getNaquadah());
		if ( Interface.objective != null )
			GUI.Label ( new Rect ( Screen.width / 2 + 125, 0, Screen.width / 2 - 125,40), "Objetivo: " + Interface.objective);
		
		// Menu
		if ( GUI.Button( new Rect (Screen.width - 200,Screen.height / 7 * 6,100,20), "Menu", this.littleButtonsStyle) ) {
			if ( menu ) {
				menu = false;	
			} else if ( !menu ) {
				menu = true;	
			}
		}
		
		if ( GetComponent<PlayerControl>().selectionArray.Count == 0 && !Interface.buildPlacingMode) {
			Building[] bs = GetComponent<PlayerControl>().player.faction.buildings;
			int i = 0;
			foreach ( Building b in bs ) {
				if ( GUI.Button ( new Rect(85 * (2+i),Screen.height / 7 * 6,60,60), new GUIContent(b.getImage(),"Construir " + b.GetType()) ) ) {
					Interface.buildPlacingMode = true;
					Interface.mw = b.instantiateBuilding(GetComponent<PlayerControl>().player);
				}
				GUI.Label (new Rect(85 *(2+i) - 20,Screen.height / 7 * 6 + 80,100,20), "Coste: " + b.getCost());
				i++;
			}
		}
		
		if ( menu ) {
			GUI.Box ( new Rect ( Screen.width - 300,Screen.height / 4 * 3 - 300, 250, 300 ), "Menu" );
			if ( GUI.Button ( new Rect ( Screen.width - 280, Screen.height / 4 * 3 - 250, 210, 30), lang.getText(35), this.littleButtonsStyle ) ) {
				menu = false;
			}
			
			if ( GUI.Button ( new Rect ( Screen.width - 280, Screen.height / 4 * 3 - 90, 210, 30), lang.getText (36), this.littleButtonsStyle ) ) {
				menu = false;
				Destroy (GameObject.Find ("Main Camera"));
				Application.LoadLevel(0);
				GameObject.Find ("Main Camera").GetComponent<MenuController>().changeScreen("MainMenu");
			}
			
			if ( GUI.Button ( new Rect ( Screen.width - 280, Screen.height / 4 * 3 - 50, 210, 30), lang.getText (37), this.littleButtonsStyle ) ) {
				Application.Quit();
			}
		}
		
		// Write the active notifications in the screen.
		for (int i = 0; i < notifications.Count; i++) {
			Notification not = Interface.notifications[i] as Notification;
			GUI.contentColor = not.color;
			notificationsStyle.alignment = not.location == 'l' ? TextAnchor.MiddleLeft : TextAnchor.MiddleCenter;
			int size = not.location == 'l' ? 13 : 30;
			notificationsStyle.fontSize = size;
			int xLocation = not.location == 'l' ? 2 : Screen.width / 4;
			int yLocation = not.location == 'l' ? Screen.height / 4 * 3 : Screen.height / 2;
			GUI.Label(new Rect(xLocation,(float)(yLocation - size * 1.5 *(i + 1)),Screen.width / 2,size * (float)1.5),not.text,notificationsStyle);
			not.counter -= 1*Time.deltaTime;

			if(not.counter <= 0) {
				Interface.notifications.RemoveAt(i);
				i--;
			}
		}
		
		GUI.contentColor = Color.white;
		
		// Changing Name
		if ( informationSelection ) {
			GUI.Box ( new Rect ( 5,Screen.height / 4 * 3 - 300, 250, 300 ), "Info" );
			GUI.contentColor = Color.black;
			GUI.Label ( new Rect ( 15,Screen.height / 4 * 3 - 280,60,20),lang.getText(44));
			this.objNameSelection.selectName = GUI.TextField ( new Rect ( 75,Screen.height / 4 * 3 - 280,170,20 ), this.objNameSelection.selectName);
			if ( this.objNameSelection is Ship ) {
				GUI.Label ( new Rect ( 15,Screen.height / 4 * 3 - 250,230,20),lang.getText(40) + (this.objNameSelection as Ship).maxHull);
				GUI.Label ( new Rect ( 15,Screen.height / 4 * 3 - 220,230,20),lang.getText(41) + (this.objNameSelection as Ship).maxShield);
				GUI.Label ( new Rect ( 15,Screen.height / 4 * 3 - 190,230,20),lang.getText(42) + (this.objNameSelection as Ship).shieldRegen + "/s");
				GUI.Label ( new Rect ( 15,Screen.height / 4 * 3 - 160,230,20),lang.getText(45) + (this.objNameSelection as Ship).minTripulation);
			} else if ( this.objNameSelection is Building ) {
				GUI.Label ( new Rect ( 15,Screen.height / 4 * 3 - 250,230,20),lang.getText(40) + (this.objNameSelection as Building).maxHull);
				GUI.Label ( new Rect ( 15,Screen.height / 4 * 3 - 220,230,20),lang.getText(43) + (this.objNameSelection as Building).hullRegen + "/s");
			}
			if ( GetComponent<PlayerControl>().selectionArray.Count == 0 || GetComponent<PlayerControl>().selectionArray.Count > 0 && !GetComponent<PlayerControl>().selectionArray.Contains(this.objNameSelection) )
				informationSelection = false;
			
			GUI.contentColor = Color.white;
		}
		
		// Selection Box
		if ( Time.time - selTime <= 0.1 ) {
			GUI.Box	(Interface.box,"",boxStyle);
		}
		
		// Selection Info
		for (int i = 0; i < GetComponent<PlayerControl>().selectionArray.Count; i++) {
			Selectionable s = GetComponent<PlayerControl>().selectionArray[i] as Selectionable;
			GUI.backgroundColor = s.controller.getPlayerColor();
			if ( GUI.Button ( new Rect(85 + 80*i,Screen.height / 7 * 6,60,60), new GUIContent(s.getImage(), s.selectName) ) ) {
				transform.position = s.transform.position + new Vector3(0,30,-15);
				transform.rotation = Quaternion.Euler (70,0,0);
			}
			GUI.backgroundColor = Color.white;
			
			if ( s is Building ) {
				GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 80,60,5), basicBar);
				GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 80,60 * (s as Building).actualHull / (s as Building).maxHull,5), hullBar);
				if ( s is Stargate ) {
					if ( s.controller.Equals(GameObject.Find("Main Camera").GetComponent<Game>().players[0]) ) {
						GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 90,60,5), basicBar);
						float maxPoints = 0;
						Player maxPlayer = GetComponent<PlayerControl>().player;
						for ( int j = 0; j < (s as Stargate).range.conversionPoints.Length; j++) {
							if ( (s as Stargate).range.conversionPoints[j] > maxPoints) {
								maxPoints = (s as Stargate).range.conversionPoints[j];
								maxPlayer = GameObject.Find ("Main Camera").GetComponent<Game>().players[j] as Player;
							}
						}
						GUI.color = maxPlayer.getPlayerColor();
						GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 90,60 * maxPoints / 100,5), basicBar);
						GUI.color = Color.white;
					} else {
						GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 90,60,5), basicBar);
						GUI.color = s.controller.getPlayerColor();
						GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 90,60 * (s as Stargate).range.conversionState / 100,5), basicBar);
						GUI.color = Color.white;
					}
				}
			}
			
			if ( (s as Ship) != null ) {
				Ship ship = s as Ship;
				GUI.Label ( new Rect (85 + 80*i,Screen.height / 7 * 6 + 60,60,20), ship.actualTripulation + " / " + ship.maxTripulation );
				GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 90,60,5), basicBar);
				GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 100,60,5), basicBar);
				GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 80,60,5), basicBar);
				GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 80,60 * ship.actualHull / ship.maxHull,5), hullBar);
				GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 90,60 * ship.actualShield / ship.maxShield,5), shieldBar);
				GUI.DrawTexture (new Rect(85 + 80*i,Screen.height / 7 * 6 + 100,60 * ship.actualEnergy / ship.maxEnergy,5), energyBar);
				
				if ( ship.controller.Equals (GetComponent<PlayerControl>().player) ) {
				
					if ( ship.attackState == Ship.AttackStates.Destroying ) {
				
						if ( GUI.Button (new Rect (85 + 80*i + 35,Screen.height / 7 * 6 - 15,15,15), "+", this.littleButtonsStyle) ) {
							ship.attackState = Ship.AttackStates.Capturing;
						}
				
					} else if ( ship.attackState == Ship.AttackStates.Capturing ){
				
						if ( GUI.Button (new Rect (85 + 80*i + 35,Screen.height / 7 * 6 - 15,15,15), "o", this.littleButtonsStyle) ) {
							ship.attackState = Ship.AttackStates.Destroying;
						}
					
					}
				
				}
			}
			
			if ( s.controller.Equals (GetComponent<PlayerControl>().player) ) {
			
				if ( GUI.Button (new Rect (85 + 80*i + 5,Screen.height / 7 * 6 - 15,15,15), "I", this.littleButtonsStyle) ) {
					this.objNameSelection = s;
					informationSelection = true;
				}
			}
			
		}
		
		if ( showInfo ) {
			GUI.DrawTexture (new Rect (Input.mousePosition.x + 5,Screen.height - Input.mousePosition.y - 10, 60, 10), basicBar);
			GUI.DrawTexture (new Rect (Input.mousePosition.x + 5,Screen.height - Input.mousePosition.y - 10, 60 * infoIndex, 10), infoBar);
			if ( infoSelection is Stargate ) {
				if ( (infoSelection as Stargate).controller.Equals(GameObject.Find("Main Camera").GetComponent<Game>().players[0]) ) {
					GUI.DrawTexture (new Rect (Input.mousePosition.x + 5,Screen.height - Input.mousePosition.y - 25, 60, 10), basicBar);
					float maxPoints = 0;
					Player maxPlayer = GetComponent<PlayerControl>().player;
					for ( int j = 0; j < (infoSelection as Stargate).range.conversionPoints.Length; j++) {
						if ( (infoSelection as Stargate).range.conversionPoints[j] > maxPoints) {
							maxPoints = (infoSelection as Stargate).range.conversionPoints[j];
							maxPlayer = GameObject.Find ("Main Camera").GetComponent<Game>().players[j] as Player;
						}
					}
					GUI.color = maxPlayer.getPlayerColor();
					GUI.DrawTexture (new Rect (Input.mousePosition.x + 5,Screen.height - Input.mousePosition.y - 25, 60 * maxPoints / 100, 10), basicBar);
					GUI.color = Color.white;
				} else {
					GUI.DrawTexture (new Rect (Input.mousePosition.x + 5,Screen.height - Input.mousePosition.y - 25, 60, 10), basicBar);
					GUI.color = (infoSelection as Stargate).controller.getPlayerColor();
					GUI.DrawTexture (new Rect (Input.mousePosition.x + 5,Screen.height - Input.mousePosition.y - 25, 60 * (infoSelection as Stargate).range.conversionState / 100, 10), basicBar);
					GUI.color = Color.white;
				}
			}
			if ( infoSelection is Ship )
				GUI.Label (new Rect (Input.mousePosition.x + 5,Screen.height - Input.mousePosition.y - 30, 60, 20), (infoSelection as Ship).actualTripulation + " / " + (infoSelection as Ship).maxTripulation);
			GUI.contentColor = infoSelection.controller.getPlayerColor();
			GUI.Label (new Rect (Input.mousePosition.x - 15,Screen.height - Input.mousePosition.y - 50, 100, 20), infoSelection.selectName);
		}
		
		if ( GUI.tooltip != "" ) {
			GUI.Label (new Rect (Input.mousePosition.x + 5,Screen.height - Input.mousePosition.y - 20, 100, 40), GUI.tooltip);	
		}
		
	}

	// Add a notification.
	public static void notification (string writing, double time, Color color,char location) {
		if ( Interface.notifications.Count < 7 ) {
			Interface.notifications.Add(new Notification(writing,time,color,location));
		} 
	}
	
	public static void cleanNotifications () {
		Interface.notifications.Clear();	
	}
	
	// Draw the selection box.
	public static void selBox (Rect box) {
		selTime = Time.time;
		Interface.box = box;
	}
	
	public static void showBar (Selectionable s) {
		if ( s is Ship ) {
			if ( (s as Ship).actualShield > 0 ) {
				infoBar = shieldBar;
				infoIndex = (s as Ship).actualShield / (s as Ship).maxShield;
			} else {
				infoBar = hullBar;
				infoIndex = (s as Ship).actualHull / (s as Ship).maxHull;
			}
		} else if ( s is Building ) {
			infoBar = hullBar;
			infoIndex = (s as Building).actualHull / (s as Building).maxHull;
		}
		infoSelection = s;
		showInfo = true;
	}
	
	public static void hideBar () {
		showInfo = false;	
	}
	
	public static void changeMapPos () {
		if ( mapState == MapStates.Small ) {
			mapState = MapStates.Big;	
		} else if ( mapState == MapStates.Big ) {
			mapState = MapStates.Small;
		}
	}
	
	public static void winGame () {
		Interface.notification(lang.getText(46),10,Color.blue,'c');
		menu = true;
		Time.timeScale = 0;
	}
	
	public static void failGame () {
		Interface.notification(lang.getText(47),10,Color.blue,'c');
		menu = true;
		Time.timeScale = 0;
	}
}
