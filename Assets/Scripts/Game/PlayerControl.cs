using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
	
	private double scrollArea;			// The area around the screen that moves the camera if we move the mouse there.
	private float scrollSpeed;			// The speed of camera's movement.
	private float rotationSpeed;		// The speed of camera's rotation.
	public int mapSize;				// The size of the map and the camera movement's limit.
	private float scrollingAngle;		// A requiered variable to move the camera.
	
	public LayerMask layerMask;
	
	private Vector3 boxSquare;
	private Rect selectionBox;
	
	public Player player;
	
	public ArrayList selectionArray;
	
	void Start () {
		// Initialization
		this.selectionArray = new ArrayList (5);
		
		this.scrollArea = 0.0075 * Screen.width;
		this.scrollSpeed = 20;
		this.rotationSpeed = 60;
		this.mapSize = GetComponent<GameLevel>().mapSize;
		this.scrollingAngle = Mathf.Tan (transform.rotation.eulerAngles.x * Mathf.PI / 180);
		StartCoroutine(soundTrack());
	}
	
	void Update () {
		
		this.cameraControl ();
		
		if ( Input.GetKeyDown (KeyCode.M) ) {
			Interface.changeMapPos();	
		}
		
		if ( Input.GetKeyDown(KeyCode.Mouse0) && mouseAvailable() ) {
			this.leftClick();	
		} else if ( Input.GetKeyDown(KeyCode.Mouse1) && mouseAvailable() ) {
			this.rightClick();	
		}
		
		if ( Input.GetKey (KeyCode.Mouse0) && mouseAvailable() ) {
			if ( Vector3.Distance (this.boxSquare,Input.mousePosition) > 5) {
				float x1 = Mathf.Min(boxSquare.x, Input.mousePosition.x);
				float y1 = Screen.height - Mathf.Max(this.boxSquare.y, Input.mousePosition.y);
				float x2 = Mathf.Abs(this.boxSquare.x - Input.mousePosition.x);
				float y2 = Mathf.Abs(this.boxSquare.y - Input.mousePosition.y);
				this.selectionBox = new Rect(x1,y1,x2,y2);
				Interface.selBox( this.selectionBox );
			}
		}
		
		if ( Input.GetKeyUp (KeyCode.Mouse0) && mouseAvailable() ) {
			if ( Vector3.Distance (this.boxSquare,Input.mousePosition) > 5) {
				if ( !Input.GetKey (KeyCode.LeftShift) ) {
					this.makeUnselection(null);
				}
				
				Selectionable[] objs = GameObject.FindObjectsOfType(typeof(Selectionable)) as Selectionable[];
				foreach (Selectionable s in objs) {
					if ( s.controller.Equals (this.player) && this.selectionBox.Contains (new Vector2 (camera.WorldToScreenPoint(s.transform.position).x,Screen.height - camera.WorldToScreenPoint(s.transform.position).y)) ) {
						if (s.selectionState == Selectionable.SelectionStates.unselected) { 
							if ( this.selectionArray.Count < 5 ) {
								makeSelection(s);
							}
						}
					}
				}	
			}
		}
		
	}
	
	private void leftClick () {
		
			this.boxSquare = Input.mousePosition;
		
			RaycastHit hit;
			Ray direction = camera.ScreenPointToRay(Input.mousePosition);
			if ( Physics.Raycast(direction,out hit,Mathf.Infinity,layerMask) && !Interface.buildPlacingMode ) {
				if ( !Minicamera.isInMinimap(Input.mousePosition) ) {
				Selectionable sel = hit.transform.root.GetComponent<Selectionable>();
				if ( sel != null ) {
					if ( sel.selectionState == Selectionable.SelectionStates.selected ) {
						if ( Input.GetKey(KeyCode.LeftShift) ) {
							this.makeUnselection(sel);
						} else {
							this.makeUnselection(null);
							if ( !sel.GetComponent<FoWagent>().isHidden )
								this.makeSelection (sel);
						}
					} else if ( !this.selectionArray.Contains(sel) ) {
						if ( !Input.GetKey(KeyCode.LeftShift) ) {
							this.makeUnselection(null);
							if ( !sel.GetComponent<FoWagent>().isHidden )
								this.makeSelection(sel);
						} else if ( this.selectionArray.Count < 5 ) {
							if ( !sel.GetComponent<FoWagent>().isHidden )
								this.makeSelection(sel);	
						}
					}
				} else if ( !Input.GetKey(KeyCode.LeftShift) ) {
					this.makeUnselection(null);
				}
				}
			}
		
			if ( Interface.buildPlacingMode ) {
				if ( Interface.mw.GetComponent<Building>().rightPlace 
				&& this.player.getNaquadah() >= Interface.mw.GetComponent<Building>().getCost() ) {
					Interface.mw.GetComponent<Building>().build();
					Interface.mw = null;
					Interface.buildPlacingMode = false;
				} else if (Interface.mw.GetComponent<Building>().rightPlace) {
					Interface.notification( "No tienes suficientes recursos.",3,Color.red,'c' );
				} else if ( Interface.mw.transform.FindChild("detect").GetComponent<SourceDetection>().source == null && Interface.mw.GetComponent<Building>() is Stargate) {
					Interface.notification( "No se puede construir en este lugar.",3,Color.red,'c' );
				} else if (Interface.mw.transform.FindChild("detect").GetComponent<SourceDetection>().source.extractor != null && Interface.mw.GetComponent<Building>() is Stargate) {
					Interface.notification( "Ya hay un Stargate en este lugar.",3,Color.red,'c' );
				} else {
					Interface.notification( "No se puede construir en este lugar.",3,Color.red,'c' );
				}
			}
		
	}
	
	private void makeSelection(Selectionable sel) {
		sel.selectionState = Selectionable.SelectionStates.selected;
		this.selectionArray.Add (sel);
	}
	
	private void makeUnselection(Selectionable sel) {
		if ( sel != null ) {
			sel.selectionState = Selectionable.SelectionStates.unselected;
			this.selectionArray.Remove(sel);
		} else {
			foreach (Selectionable s in this.selectionArray) {
				s.selectionState = Selectionable.SelectionStates.unselected;
			}
			this.selectionArray.Clear ();
		}
	}
	
	private void orderMove (Vector3 destination) {
		ArrayList ships = new ArrayList();
		float dist = 7;
		int direction = -1;
		
		foreach ( Selectionable s in this.selectionArray ) {
			if ( s.controller.Equals (this.player) && (s as Ship) != null ) {
				ships.Add (s);
			}
		}
		
		if ( ships.Count % 2 == 1 || ships.Count > 2) {
			(ships[0] as Ship).move ( new Vector3 ( destination.x ,0, destination.z ), true );
			
			for ( int i = 1; i < ships.Count; i++ ) {
				Ship ship = ships[i] as Ship;
				ship.move ( new Vector3 ( destination.x + dist * direction * (Mathf.Floor( (i + 1) / 2) - Mathf.Floor (i / 3) * 2) ,0, destination.z + dist * direction * Mathf.Floor (i / 3) ), true );
				direction *= -1;
			}
		} else if ( ships.Count != 0 ) {
				for ( int i = 0; i < ships.Count; i++ ) {
				Ship ship = ships[i] as Ship;
				ship.move ( new Vector3 ( destination.x + dist / 2 * direction ,0, destination.z ), true );
				direction *= -1;
			}
		}
	}
	
	private void orderAttack (GameObject objective) {
		foreach ( Selectionable s in this.selectionArray ) {
			if ( (s as Ship) != null && s.controller.Equals (this.player)) {
				(s as Ship).primaryWeaponsFire (objective);
			}
		}
	}
	
	private void rightClick() {
		RaycastHit hit;
		Ray direction = camera.ScreenPointToRay(Input.mousePosition);
		if ( Physics.Raycast(direction,out hit,Mathf.Infinity,layerMask) ) {
			Selectionable sel = hit.transform.root.GetComponent<Selectionable>();
			if ( sel != null && sel.gameObject.layer != 16 && !sel.controller.getTeam().Equals (this.player.getTeam()) ) {
				if ( sel is Ship && (sel as Ship).tripulationState == Ship.TripulationStates.Empty && this.selectionArray.Count == 1 
					&& (this.selectionArray[0] as Selectionable) is Ship && (this.selectionArray[0] as Ship).attackState == Ship.AttackStates.Capturing) {
					
					StartCoroutine((this.selectionArray[0] as Ship).giveTripulation(sel as Ship,(this.selectionArray[0] as Ship).actualTripulation / 2));
				} else {
					this.orderAttack (sel.gameObject);
					sel.attackSelection();
				}
			} else if ( sel == null ) {
				this.orderMove (hit.point);	
			} else if ( sel is Ship && sel.controller.getTeam().Equals (this.player.getTeam()) && this.selectionArray.Count == 1 
				&& (this.selectionArray[0] as Selectionable) is Ship) {

				StartCoroutine((this.selectionArray[0] as Ship).giveTripulation(sel as Ship,(this.selectionArray[0] as Ship).actualTripulation / 2));
				
			}
		}
		
		if ( Interface.buildPlacingMode ) {
			Destroy (Interface.mw);
			Interface.mw = null;
			Interface.buildPlacingMode = false;
		}
	}
	
	private void cameraControl() {
		if ( !Input.GetKey (KeyCode.Mouse2) && !Input.GetKey (KeyCode.Mouse0) ) {
			Screen.lockCursor = false;
			
 			if ( (Input.mousePosition.x < scrollArea || Input.GetKey (KeyCode.LeftArrow)) && transform.position.x > -mapSize )
 				transform.Translate(new Vector3(-scrollSpeed * Time.deltaTime,0,0));
 		
 			if ( (Input.mousePosition.x > Screen.width - scrollArea || Input.GetKey (KeyCode.RightArrow)) && transform.position.x < mapSize ) 
 				transform.Translate(new Vector3(scrollSpeed * Time.deltaTime,0,0));
 		
 			if ( (Input.mousePosition.y < scrollArea || Input.GetKey (KeyCode.DownArrow)) && transform.position.z > -mapSize - 60 ) 
 				transform.Translate(new Vector3(0,-scrollingAngle * scrollSpeed * Time.deltaTime,-scrollSpeed * Time.deltaTime));
 	
 			if ( (Input.mousePosition.y > Screen.height - scrollArea || Input.GetKey (KeyCode.UpArrow)) && transform.position.z < mapSize ) 
 				transform.Translate(new Vector3(0,scrollingAngle * scrollSpeed * Time.deltaTime,scrollSpeed * Time.deltaTime));
 		} else if ( !Input.GetKey (KeyCode.Mouse0) ){
			
			RaycastHit clic;
			
			Physics.Raycast (camera.ScreenPointToRay(new Vector3(Screen.width / 2,Screen.height / 2, 0)),out clic);
			
			float difference = Input.mousePosition.x - Screen.width / 2;
			Screen.lockCursor = true;
			difference = difference == 0 ? 1 : -1;
			transform.RotateAround(clic.point,Vector3.up, (difference * rotationSpeed * Time.deltaTime));
 		
		}

		// Camera zoom.
		if ( (transform.position.y > 10 && Input.GetAxis("Wheel") > 0) || (transform.position.y < 60 && Input.GetAxis("Wheel") < 0) ) {
			transform.Translate (new Vector3 (0,-Input.GetAxis("Wheel") / 10 *Time.deltaTime,0),Space.World);
		}
		if ( transform.position.y < 10 ) {
			transform.position = new Vector3 (transform.position.x,10,transform.position.z);
		} else if ( transform.position.y > 60 ) {
			transform.position = new Vector3 (transform.position.x,60,transform.position.z);
		}
		
		// Camera reset.
		if ( Input.GetKeyDown (KeyCode.Space) ) {
			transform.position = new Vector3(transform.position.x, 55, transform.position.z);
			transform.rotation = Quaternion.Euler ( new Vector3 (70,0,0) );
		}
		if ( Input.GetKeyDown (KeyCode.KeypadPlus) ) {
			transform.position = new Vector3(transform.position.x, 18, transform.position.z);
		}
	}
	
	private bool mouseAvailable () {
		return Input.mousePosition.y > Screen.height / 4 && 
				( !Interface.informationSelection || Input.mousePosition.x > 255 && Input.mousePosition.y > Screen.height / 4 )
				&& ( !Interface.menu || Input.mousePosition.x < Screen.width - 280 && Input.mousePosition.y > Screen.height / 4 );
	}
	
	private IEnumerator soundTrack() {
		while (true) {
			GameObject s = Sound.playSound( "Music/stargate_bow",transform.position,0.2F);
			s.transform.parent = transform;
			yield return new WaitForSeconds(s.audio.clip.length);
			s = Sound.playSound( "Music/The Rescue",transform.position,0.2F);
			s.transform.parent = transform;
			yield return new WaitForSeconds(s.audio.clip.length);
		}
	}
}
